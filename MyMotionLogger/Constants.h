//
//  Constants.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef MyMotionLogger_Constants_h
#define MyMotionLogger_Constants_h



typedef struct {
    int hours;
    int minutes;
    int seconds;
} timeStruct ;




typedef enum {
    kGPSLatitude,
    kGPSLongitude,
    kGPSTime,
    kGPSTimeStamp,
    kGPSCourse,
    kGPSSpeed,
    kGPSAltitude,
    kGPSVerticalAccuracy,
    kGPSHorizontalAccuracy,
    kHeadingMagneticHeading,
    kHeadingTrueHeading,
    kHeadingHeadingAccuracy,
    kHeadingX,
    kHeadingY,
    kHeadingZ,
    kHeadingTime,
    kHeadingTimeStamp,
    kGyroTime,
    kGyroXData,
    kGyroYData,
    kGyroZData,
    kMagnetometerTime,
    kMagnetometerXData,
    kMagnetometerYData,
    kMagnetometerZData,
    kAccelerometerTime,
    kAccelerometerXData,
    kAccelerometerYData,
    kAccelerometerZData
} EventDataName;


#endif
