//
//  Event.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 10/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * gpsDesiredAccuracy;
@property (nonatomic, retain) NSNumber * nPointsAccelerometer;
@property (nonatomic, retain) NSNumber * nPointsGPS;
@property (nonatomic, retain) NSNumber * nPointsGyroscope;
@property (nonatomic, retain) NSNumber * nPointsHeading;
@property (nonatomic, retain) NSNumber * nPointsMagnetometer;
@property (nonatomic, retain) NSNumber * samplingFrequency;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSDate * stopTime;
@property (nonatomic, retain) NSString * userNotes;

@end
