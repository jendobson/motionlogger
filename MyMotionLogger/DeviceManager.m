//
//  DeviceManager.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DeviceManager.h"
#import "SettingsModel.h"

#import "GyroscopeDataCollector.h"
#import "AccelerometerDataCollector.h"
#import "MagnetometerDataCollector.h"
#import "HeadingDataCollector.h"
#import "GPSDataCollector.h"

#import "CollectionEvent.h"

@implementation DeviceManager

@synthesize motionManager = _motionManager;
@synthesize locationManager = _locationManager;

@synthesize gyroscopeDataCollector = _gyroscopeDataCollector;
@synthesize headingDataCollector = _headingDataCollector;
@synthesize accelerometerDataCollector = _accelerometerDataCollector;
@synthesize magnetometerDataCollector = _magnetometerDataCollector;
@synthesize gpsDataCollector = _gpsDataCollector;

@synthesize opQ = _opQ;


-(id) init
{
    self = [super init];
    
    if (self)
    {
        // Customized init steps go here
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        
        self.motionManager = [[CMMotionManager alloc] init];
        
        _gyroscopeDataCollector = [[GyroscopeDataCollector alloc] init];
        _headingDataCollector = [[HeadingDataCollector alloc] init];
        _gpsDataCollector = [[GPSDataCollector alloc] init];
        _accelerometerDataCollector = [[AccelerometerDataCollector alloc] init];
        _magnetometerDataCollector = [[MagnetometerDataCollector alloc] init];
    }

    return self;
    
}

#pragma mark - SET/GET methods
-(void) setSamplingFrequency:(double)samplingFrequency
{
    if (samplingFrequency>100 | samplingFrequency<0)
        //TODO: handle this error
        NSLog(@"ERROR: Sampling frequency out of range!");
    else {
        [[SettingsModel sharedSettingsModel] setSamplingFrequency:samplingFrequency];
    }

}
-(double)samplingFrequency
{
    return [[SettingsModel sharedSettingsModel] samplingFrequency];
}
-(void) setGpsLocationAccuracy:(CLLocationAccuracy)gpsLocationAccuracy
{
    [[SettingsModel sharedSettingsModel] setGpsLocationAccuracy:gpsLocationAccuracy];
}

-(CLLocationAccuracy) gpsLocationAccuracy
{
    return [[SettingsModel sharedSettingsModel] gpsLocationAccuracy];
}


-(BOOL) collectsGyroscopeData
{
    return [[SettingsModel sharedSettingsModel] collectsGyroscopeData];
}
-(void) setCollectsGyroscopeData:(BOOL)collectsGyroscopeData
{
    [[SettingsModel sharedSettingsModel] setCollectsGyroscopeData:collectsGyroscopeData];
}
-(BOOL) collectsAccelerometerData
{
    return  [[SettingsModel sharedSettingsModel] collectsAccelerometerData];
}
-(void) setCollectsAccelerometerData:(BOOL)collectsAccelerometerData
{
    [SettingsModel sharedSettingsModel].collectsAccelerometerData = collectsAccelerometerData;
}
-(BOOL) collectsMagnetometerData
{
    return [[SettingsModel sharedSettingsModel] collectsMagnetometerData];
}
-(void) setCollectsMagnetometerData:(BOOL)collectsMagnetometerData
{
    [SettingsModel sharedSettingsModel].collectsMagnetometerData = collectsMagnetometerData;
}
-(BOOL) collectsGPSData
{
    return [[SettingsModel sharedSettingsModel] collectsGPSData];
}
-(void) setCollectsGPSData:(BOOL)collectsGPSData
{
    [SettingsModel sharedSettingsModel].collectsGPSData = collectsGPSData;
}
-(BOOL) collectsHeadingData
{
    return [[SettingsModel sharedSettingsModel] collectsHeadingData];
}
-(void) setCollectsHeadingData:(BOOL)collectsHeadingData
{
    [SettingsModel sharedSettingsModel].collectsHeadingData = collectsHeadingData;
}

#pragma mark - methods
-(void) reset
{
    [self.gpsDataCollector freeData];
    [self.headingDataCollector freeData];
    [self.gyroscopeDataCollector freeData];
    [self.magnetometerDataCollector freeData];
    [self.accelerometerDataCollector freeData];
}
-(void) startDataCollectionInEvent:(CollectionEvent *)event
{
    self.motionManager.gyroUpdateInterval = 1/self.samplingFrequency;
    self.motionManager.accelerometerUpdateInterval = 1/self.samplingFrequency;
    self.motionManager.magnetometerUpdateInterval = 1/self.samplingFrequency;
    self.locationManager.desiredAccuracy = self.gpsLocationAccuracy;
    
    self.opQ = [[NSOperationQueue alloc] init];
    
    
    event.startTime = [NSDate date];
    
    if (self.collectsAccelerometerData)
    {[self.accelerometerDataCollector startDataCollectionInEvent:event];
    }
    
    if (self.collectsGyroscopeData)
    {[self.gyroscopeDataCollector startDataCollectionInEvent:event];
    }
    
    if (self.collectsMagnetometerData)
        
    {[self.magnetometerDataCollector startDataCollectionInEvent:event];}

    
    if (self.collectsGPSData)
    {
        [self.gpsDataCollector startDataCollectionInEvent:event];
    }
    
    if (self.collectsHeadingData)
    {
        [self.headingDataCollector startDataCollectionInEvent:event];   
    }
    
  }

-(void) stopDataCollectionInEvent:(CollectionEvent *)event
{
    
    [self.opQ waitUntilAllOperationsAreFinished];
   
    [self.gpsDataCollector stopDataCollectionInEvent:event];
    
    [self.headingDataCollector stopDataCollectionInEvent:event];
    
    [self.accelerometerDataCollector stopDataCollectionInEvent:event];
    
    [self.gyroscopeDataCollector stopDataCollectionInEvent:event];
    
    [self.magnetometerDataCollector stopDataCollectionInEvent:event];
    
    self.opQ = nil;
    
}

#pragma mark - CLLocationManagerDelegate methods
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation 
{
    //if (self.collectsGPSData)
        [self.gpsDataCollector doAddLocationToData:newLocation];
    
}
- (void) locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    //if (self.collectsHeadingData)
        [self.headingDataCollector doAddHeadingToData:newHeading];
    
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Location Data Collection Failed"
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    // TODO: notify event that collection failed?
}

@end
