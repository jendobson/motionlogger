//
//  AcquireDataViewController.m
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AcquireDataViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "CollectionEvent.h"
#import "Event+Create.h"
#import "SettingsModel.h"

#import "EventSummaryViewController.h"

typedef int startButtonState;

const enum startButtonState{
    kStart,
    kStop,
    kWritingData
} states;

@interface AcquireDataViewController ()

@end

@implementation AcquireDataViewController

@synthesize timeSinceStartLabel;
@synthesize gpsNPointsLabel;
@synthesize headingNPointsLabel;
@synthesize accelerometerNPointsLabel;
@synthesize magnetometerNPointsLabel;
@synthesize gyroscopeNPointsLabel;
@synthesize timeOutInTimeLabel;
@synthesize startStopButton;
@synthesize collectionActivityIndicator;
@synthesize updateTimer = _updateTimer;

# pragma mark - SET/GET Methods

  
#pragma mark - methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization  

    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    // Make self know to appDelegate
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.advc = self;
    
    
    if ([CollectionEvent sharedCollectionEvent].isCollectingData)
    {
        [self toggleStartStopButton:kStart];
        [self beginScreenUpdates];
    }
    else {
        [self toggleStartStopButton:kStop];
    }

}

-(void)viewWillDisappear:(BOOL)animated
{
    // Remove self from appDelegate
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.advc = nil;
    
    [self terminateScreenUpdates];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    self.startStopButton.layer.borderWidth = 0.5f;
    self.startStopButton.layer.cornerRadius = 10.0f;
    [self toggleStartStopButton:kStart]; // toggle button to cache STOP image
    [self toggleStartStopButton:kStop];
    self.collectionActivityIndicator.hidesWhenStopped = YES;
    [self.collectionActivityIndicator setActivityIndicatorViewStyle: UIActivityIndicatorViewStyleWhiteLarge];
    

}


-(void) prepareForBackground
{

    [[NSNotificationCenter defaultCenter] removeObserver:self];

    
    [self toggleStartStopButton:kStop];
    
    [self terminateScreenUpdates];
}

-(void) prepareForForeground
{
    
    while ([CollectionEvent sharedCollectionEvent].isWritingData )
    {
        
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];
        
        
    }
             
         
    if ([self.navigationController.visibleViewController isEqual:self])
    {
    //    NSLog(@"collection event: %@",[CollectionEvent sharedCollectionEvent].persistentEvent );
    //Push the summary view controller
    EventSummaryViewController* esvc = [[EventSummaryViewController alloc] initWithEvent:[CollectionEvent sharedCollectionEvent].persistentEvent];
    
    [self.navigationController pushViewController:esvc animated:YES];
        
        esvc.title = @"Last Collection";
        
        

    }
}

- (void)viewDidUnload
{
 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
  
    [self setStartStopButton:nil];
    [self setCollectionActivityIndicator:nil];
    
    [self setTimeSinceStartLabel:nil];
    [self setGpsNPointsLabel:nil];
    [self setHeadingNPointsLabel:nil];
    [self setAccelerometerNPointsLabel:nil];
    [self setMagnetometerNPointsLabel:nil];
    [self setGyroscopeNPointsLabel:nil];
    [self setTimeOutInTimeLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) toggleStartStopButton:(startButtonState)state
{
    
  
    if (state == kStart)
    {
        
            
            self.timeOutInTimeLabel.text = @"";
        
            [self.collectionActivityIndicator stopAnimating];
        
            [self.startStopButton setImage:[UIImage imageNamed:@"StopIcon.png"] forState:UIControlStateNormal];
        
         
            [self.startStopButton setEnabled:YES];
        
    }
    else if (state == kStop) {
        self.timeOutInTimeLabel.text = @"";
        
        [self.collectionActivityIndicator stopAnimating];
        
        [self.startStopButton setImage:[UIImage imageNamed:@"GoIcon.png"] forState:UIControlStateNormal];
        
        
        [self.startStopButton setEnabled:YES];
    }
    else if (state == kWritingData)
    {
        [self.startStopButton setEnabled:NO];
        self.timeOutInTimeLabel.text = @"Writing Data.  Please wait...";
        [self.collectionActivityIndicator startAnimating];
        
         
    }

}


-(void)respondToDataStartedWriting:(NSNotification*)notification
{
    [self toggleStartStopButton:kWritingData];
}

-(void)respondToDataFinishedWriting:(NSNotification*)notification
{
  
    
    [self collectionDataFinishedWriting];
}



-(void)collectionDataFinishedWriting
{
    // push event summary view
    
    EventSummaryViewController* esvc = [[EventSummaryViewController alloc] initWithEvent:[CollectionEvent sharedCollectionEvent].persistentEvent];
    
    [self.navigationController pushViewController:esvc animated:YES];
    esvc.title = @"Last Collection";
    // Remove self from observer list
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (IBAction)startStopButtonPressed:(UIButton *)sender {

    /*
    if (nil == self.collectionEvent)
    {
        self.collectionEvent = [[CollectionEvent alloc] init];
        
    }
    */
    
    if (NO == [CollectionEvent sharedCollectionEvent].isCollectingData)
    {
        
        if ([SettingsModel sharedSettingsModel].collectsGPSData |
            [SettingsModel sharedSettingsModel].collectsAccelerometerData |
            [SettingsModel sharedSettingsModel].collectsGyroscopeData |
            [SettingsModel sharedSettingsModel].collectsHeadingData |
            [SettingsModel sharedSettingsModel].collectsMagnetometerData)
        {// Sign up to receive data collection alerts
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(respondToDataStartedWriting:) name:@"Data Started Writing" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(respondToDataFinishedWriting:) name:@"Data Finished Writing" object:nil];
            
            
            [[CollectionEvent sharedCollectionEvent] startCollection];
            [self toggleStartStopButton:kStart];
            
            [self beginScreenUpdates];
        }
        else 
        {
            UIAlertView *warningDlg = [[UIAlertView alloc] initWithTitle:@"All Sensors Disabled" message:@"All sensors are disabled.  Please enable at least one sensor in the configuration view.  (Press the ""i"" icon in the upper right corner of this screen" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [warningDlg show];
        }

        
    }
    else {
        [self terminateScreenUpdates];
        
        [[CollectionEvent sharedCollectionEvent] stopCollection];
       
    }
    
    
}

-(void)beginScreenUpdates
{
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateScreen:) userInfo:nil repeats:YES];
    [self.updateTimer fire];

}
-(void)updateScreen:(NSTimer*)timer
{
    
    // Update collection timer
    NSTimeInterval secondsSinceCollectionStart = (0-[[CollectionEvent sharedCollectionEvent].startTime timeIntervalSinceNow]);
  
    int hrs = floor(secondsSinceCollectionStart/3600);
    int mins = floor((secondsSinceCollectionStart-3600*hrs)/60);
    int secs = floor((secondsSinceCollectionStart-3600*hrs - 60*mins));
    
    
    self.timeSinceStartLabel.text = [NSString stringWithFormat:@"%02i:%02i:%02i",hrs,mins,secs];
    
    // Update time until timeout
    NSTimeInterval secondsUntilTimeOut = [[[CollectionEvent sharedCollectionEvent].startTime dateByAddingTimeInterval:[CollectionEvent sharedCollectionEvent].timeOutInterval] timeIntervalSinceNow];
    
    int hrsTO = floor(secondsUntilTimeOut/3600);
    int minsTO = floor((secondsUntilTimeOut-3600*hrsTO)/60);
    int secsTO = floor((secondsUntilTimeOut-3600*hrsTO - 60*minsTO));
    
    self.timeOutInTimeLabel.text = [NSString stringWithFormat:@"Data collection will time out in: %02i:%02i:%02i",hrsTO,minsTO,secsTO];
    
    int magNPoints = [[[CollectionEvent sharedCollectionEvent] getNPoints:kMagnetometerXData] intValue];
    if (0!=magNPoints)
        self.magnetometerNPointsLabel.text = [NSString stringWithFormat:@"%i",
                                              magNPoints];
    
    int accNPoints = [[[CollectionEvent sharedCollectionEvent] getNPoints:kAccelerometerTime] intValue];
    if (0!=accNPoints)
        self.accelerometerNPointsLabel.text = [NSString stringWithFormat:@"%i",
                                           accNPoints];
    
    int gyrNPoints = [[[CollectionEvent 
                        sharedCollectionEvent] 
                       getNPoints:kGyroTime] 
                      intValue];
    if (0!=gyrNPoints)
        self.gyroscopeNPointsLabel.text = [NSString stringWithFormat:@"%i",
                                       gyrNPoints];
    
    
    int gpsNPoints = [[[CollectionEvent 
                        sharedCollectionEvent] 
                       getNPoints:kGPSLatitude] 
                      intValue];
    if (0!=gpsNPoints)
        self.gpsNPointsLabel.text = [NSString stringWithFormat:@"%i",
                                       gpsNPoints];
    
    int headNPoints = [[[CollectionEvent sharedCollectionEvent] getNPoints:kHeadingTime] intValue];
    if (0!=headNPoints)
        self.headingNPointsLabel.text = [NSString stringWithFormat:@"%i",headNPoints];
    
}
-(void)terminateScreenUpdates
{
    self.timeSinceStartLabel.text = @"00:00:00";
    self.gpsNPointsLabel.text = @"---";
    self.headingNPointsLabel.text = @"---";
    self.gyroscopeNPointsLabel.text = @"---";
    self.magnetometerNPointsLabel.text = @"---";
    self.accelerometerNPointsLabel.text = @"---";
    self.timeOutInTimeLabel.text = @"";
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    
}

@end
