//
//  CollectionEvent.h
//  DataLogger
//
//  Created by Jennifer Dobson on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceManager.h"
#import "Constants.h"

#import "CWLSynthesizeSingleton.h"

@class DeviceManager, UserMark, Event;


@interface CollectionEvent : NSObject
{
    __block UIBackgroundTaskIdentifier bgTask;
}


// API properties
@property (nonatomic) BOOL collectsGPSData;
@property (nonatomic) BOOL collectsHeadingData;
@property (nonatomic) BOOL collectsGyroscopeData;
@property (nonatomic) BOOL collectsMagnetometerData;
@property (nonatomic) BOOL collectsAccelerometerData;
@property (nonatomic,strong) Event* persistentEvent;

// Settings
@property (nonatomic) NSTimeInterval timeOutInterval;
@property (nonatomic) double samplingFrequency;
@property (nonatomic) CLLocationAccuracy gpsAccuracy;
@property (nonatomic, readonly) NSString* gpsAccuracyString;

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, readonly) NSTimeInterval upTimeAtStart;
@property (nonatomic, strong) NSDate *stopTime;
@property (nonatomic) BOOL isCollectingData;
@property (nonatomic) BOOL isWritingData;
@property (nonatomic,readonly) NSString* name;
@property (nonatomic, readonly,strong) NSDictionary* currentReading;
@property (nonatomic, readonly, strong) NSDictionary* statistics;


@property (nonatomic,strong) NSTimer *timer;
@property (nonatomic, strong) DeviceManager *deviceManager;
@property (nonatomic, strong) NSMutableArray *userMarks;


@property (nonatomic, readonly) NSOperationQueue *opQ;


// API Methods
-(void) startCollection;
-(void) stopCollection;
-(void) addUserMark:(UserMark*)mark;
-(void)respondToCollectionTimer:(NSTimer*)theTimer;


-(void) reset;

-(NSData*) getEventData:(EventDataName)eventDataName;
-(NSNumber*) getNPoints:(EventDataName)eventDataName;


+(NSData*) getEventData:(EventDataName)eventDataName forEvent:(CollectionEvent*)event;


CWL_DECLARE_SINGLETON_FOR_CLASS(CollectionEvent)

@end

/* Post-process methods
 -(void) writeToCSV;
 -(void) writeToMAT;
 -(void) writeToGPX;
 -(void) writeToKML;
 -(void) deleteEvent;
 -(void) setGPSAccuracy;
 -(void) emailEvent;
 -(void) showEventOnMap;

*/