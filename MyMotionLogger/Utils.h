//
//  Utils.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Utils : NSObject

+(NSArray*)dataArrayFromData:(NSData*)data;

@end
