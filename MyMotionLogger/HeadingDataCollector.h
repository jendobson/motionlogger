//
//  HeadingDataCollector.h
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationDataCollector.h"

@interface HeadingDataCollector : LocationDataCollector


@end

/*
 @property (nonatomic,readonly) int nBytesInMagneticHeading;
 @property (nonatomic,readonly) int nBytesInTrueHeading;
 @property (nonatomic,readonly) int nBytesInHeadingAccuracy;
 @property (nonatomic,readonly) int nBytesInX;
 @property (nonatomic,readonly) int nBytesInY;
 @property (nonatomic,readonly) int nBytesInZ;

*/