//
//  EventsListTableViewController.m
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventsListTableViewController.h"
#import "AppDelegate.h"
#import "Event.h"
#import "Event+Create.h"
#import "EventSummaryViewController.h"


@interface EventsListTableViewController ()

@end

@implementation EventsListTableViewController
@synthesize navigationBar = _navigationBar;

@synthesize editButton = _editButton;
@synthesize eventsArray = _eventsArray;
@synthesize editState = _editState;

-(void)setEditState:(BOOL)editState
{
    if (editState)
    {
        _editState = YES;
        self.editButton.title = @"Done";
        [self.tableView setEditing:YES animated:YES];
        
        self.navigationBar.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Delete All" style:UIBarButtonItemStyleDone target:self action:@selector(deleteAllData:)];

    }
    else {
        _editState = NO;
        [self.tableView setEditing:NO animated:YES];
        
        self.editButton.title = @"Edit";
        self.navigationBar.leftBarButtonItem = nil;   
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    
}

-(void)viewWillAppear:(BOOL)animated
{

    // Log the file sizes
    /*
     NSFileManager *manager = [NSFileManager defaultManager];
   
    NSURL *documentsDirectory = [manager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDirectory].lastObject;
    NSDictionary *docsDirAttr = [manager attributesOfItemAtPath:[documentsDirectory path] error:nil];
    
    NSLog(@"documentsDirectory attributes: %@",docsDirAttr);

    NSArray *contents = [manager subpathsOfDirectoryAtPath:[documentsDirectory path] error:nil];
                         
    NSLog(@"contents: %@",contents);
    
    for (int i=0; i<contents.count; i++) {
        NSString *path = [contents objectAtIndex:i];
        NSDictionary *attributes = [manager attributesOfItemAtPath:[[NSURL URLWithString:path relativeToURL:documentsDirectory] path] error:nil];
        NSLog(@"subdir: %@, size: %i",path,[[attributes objectForKey:@"NSFileSize"] intValue]); 
    }
    
    */
    
    [self updateEventsArray];
    self.editState = NO;
    
    

}
-(void)viewWillDisappear:(BOOL)animated
{
    self.editState = NO;
}
- (void)viewDidUnload
{
    self.editState = NO;
    [self setEditButton:nil];
    [self setNavigationBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    // Release events array from managed object context
    self.eventsArray = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - helper functions
-(void) updateEventsArray
{
    //NSLog(@"updateEventsArray");
    // Get the events that have been logged
    NSManagedObjectContext *appcontext = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"]; 
    
    NSSortDescriptor *sortDescriptor =[NSSortDescriptor sortDescriptorWithKey:@"startTime"
                                                                    ascending:YES];
    
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor]; 
    
    NSError *error;
    self.eventsArray = [appcontext executeFetchRequest:request error:&error];
    
    [self.tableView reloadData];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.eventsArray.count;
}

-(Event*)getEventForIndexPath:(NSIndexPath*)indexPath
{
    NSInteger totalEvents = self.eventsArray.count;
    NSInteger eventIndexForThisRow = totalEvents - indexPath.row - 1;
    
    Event* event = (Event*)[self.eventsArray objectAtIndex:eventIndexForThisRow];
    
    return event;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"EventCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    NSDateFormatter *dateFormatter = [Event getDateFormatterForReadableText];
    
    Event* event = [self getEventForIndexPath:indexPath];
    
    NSString *startTimeText = [dateFormatter stringFromDate:event.startTime];
    
    cell.textLabel.text = startTimeText;
    
    
    // Get approximate file sizes
    NSFileManager *manager = [NSFileManager defaultManager];
    NSDictionary *gpsAttributes = [manager attributesOfItemAtPath:[event.gpsFileURL path] error:nil];
    NSDictionary *headingAttributes = [manager attributesOfItemAtPath:[event.headingFileURL path] error:nil];
    NSDictionary *magnetometerAttributes = [manager attributesOfItemAtPath:[event.magnetometerFileURL path] error:nil];
    NSDictionary *accelerometerAttributes = [manager attributesOfItemAtPath:[event.accelerometerFileURL path] error:nil];
    NSDictionary *gyroscopeAttributes = [manager attributesOfItemAtPath:[event.gyroscopeFileURL path] error:nil];
    
    int totalSize = [[gpsAttributes objectForKey:NSFileSize] intValue] + [[headingAttributes objectForKey:NSFileSize] intValue] + [[magnetometerAttributes objectForKey:NSFileSize] intValue] + [[accelerometerAttributes objectForKey:NSFileSize] intValue] + [[gyroscopeAttributes objectForKey:NSFileSize] intValue];
    
    
    
    if (totalSize < 1000000)
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%i KB",(int)round(totalSize/1000)];
    else {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%i MB",(int)round(totalSize/1000000)];
    }
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Remove the event from the data store
        Event* eventToDelete = [self getEventForIndexPath:indexPath];
        
        NSManagedObjectContext *appcontext = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
        [appcontext deleteObject:eventToDelete];
        NSError *error;
        [appcontext save:&error];
        
        
        [self updateEventsArray];
        
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    Event* selectedEvent = [self getEventForIndexPath:indexPath];
    EventSummaryViewController* esvc = [[EventSummaryViewController alloc] initWithEvent:selectedEvent];
    
    //NSLog(@"selectedEvent: %@",selectedEvent);
    [self.navigationController pushViewController:esvc animated:YES];
}


- (IBAction)editButtonPressed:(UIBarButtonItem *)sender {
    if (self.editState == NO) 
    {
        self.editState = YES;
    }
    else 
    {
        self.editState = NO;
    }
}

- (IBAction)deleteAllData:(id)sender {
    
    
    UIAlertView *warningDlg = [[UIAlertView alloc] initWithTitle:@"Delete all?" message:@"You are about to delete all data.  Press ""OK"" to continue or ""Cancel"" to abort" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    [warningDlg show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //NSLog(@"button index: %i",buttonIndex);
    
    if (buttonIndex==1)
    {
        NSArray *eventsToDelete;
        
        // Get the events that have been logged
        NSManagedObjectContext *appcontext = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"]; 
        
        NSError *error;
        eventsToDelete = [appcontext executeFetchRequest:request error:&error];
        
        
        for (int i = 0; i<eventsToDelete.count; i++) {
            [appcontext deleteObject:[eventsToDelete objectAtIndex:i]];
        }
        
        [appcontext save:&error];
        
        [self updateEventsArray];
        
        self.editState = NO;
         
    }
    else {
        [self updateEventsArray]; 
    }
    
}
@end
