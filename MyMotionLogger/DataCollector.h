//
//  DataCollector.h
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@class CollectionEvent;

@interface DataCollector : NSObject 

@property (nonatomic, readonly) int nBytesInData;
@property (nonatomic, readonly) int nPoints;
@property (nonatomic, readonly) NSString *sensorType;


-(int) nbytesInDataField:(NSString*)dataField;
-(void)freeData;
-(void) startDataCollectionInEvent:(CollectionEvent *)event;

-(void) stopDataCollectionInEvent:(CollectionEvent*)event;

// Template Methods for Subclasses
-(void)doStartDataCollectionInEvent:(CollectionEvent*)event;
-(void)doStopDataCollectionInEvent:(CollectionEvent*)event;

-(NSData*)bytesForDataField:(EventDataName)dataFieldName;


@end
