//
//  Utils.m
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSArray*)dataArrayFromData:(NSData*)data
{
    int datalength = [data length]/sizeof(double);
    double *dataptr = (double*)[data bytes];
    
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:datalength];
    for (int i=0; i<datalength; i++) {
        [dataArray addObject:[NSNumber numberWithDouble:dataptr[i]]];
    }
    return dataArray;
    
}

@end
