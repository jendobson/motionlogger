//
//  GPSDataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPSDataCollector.h"
#import "Constants.h"
#import "DeviceManager.h"
#import "CollectionEvent.h"

@implementation GPSDataCollector

-(NSString *)sensorType
{
    return @"GPS";
}

-(NSString*)getDataNameFromKey:(EventDataName)dataNameKey
{
    NSString *dataName;
    if (dataNameKey == kGPSCourse)
        dataName = @"course";
    if (dataNameKey == kGPSAltitude)
        dataName = @"altitude";
    if (dataNameKey == kGPSHorizontalAccuracy)
        dataName = @"horizontalAccuracy";
    if (dataNameKey == kGPSLatitude)
        dataName = @"latitude";
    if (dataNameKey == kGPSLongitude)
        dataName = @"longitude";
    if (dataNameKey == kGPSSpeed)
        dataName = @"speed";
    if (dataNameKey == kGPSTime)
        dataName = @"time";
    if (dataNameKey == kGPSTimeStamp)
        dataName = @"timestamp";
    if (dataNameKey == kGPSVerticalAccuracy)
        dataName = @"verticalAccuracy";
    
    return dataName;
}

- (void) doAddLocationToData:(CLLocation*)newLocation
{
    [self.measurements addObject:newLocation];
    [self.time addObject:[NSNumber numberWithDouble:[[NSProcessInfo processInfo] systemUptime]]];
}


-(void) doStartDataCollectionInEvent:(CollectionEvent*)event
{

    if ([event.deviceManager.locationManager locationServicesEnabled])
    {
        [event.deviceManager.locationManager startUpdatingLocation];
    }
    else
    {    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Location Services are not available."
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
     

}

-(void) doStopDataCollectionInEvent:(CollectionEvent*)event
{
    [event.deviceManager.locationManager stopUpdatingLocation];
}


-(NSData*)bytesForDataField:(EventDataName)dataFieldName
{
    NSMutableData* returnData = [NSMutableData dataWithCapacity:self.measurements.count];
    
    
    if (dataFieldName == kGPSTime)
            for (int i=0;i<self.time.count;i++)
            {
                double* timePtr;
                double time = [[self.time objectAtIndex:i] doubleValue];
                timePtr = &time;
                [returnData appendData:[NSData dataWithBytes:timePtr length:sizeof(double)]];
            }
    else 
    {
        double *dataBytes = malloc(sizeof(double)*self.measurements.count);
        for (int i=0;i<self.measurements.count;i++)
        {
            CLLocation *thisLocation = [self.measurements objectAtIndex:i];
            if (dataFieldName == kGPSTimeStamp)
            {
                NSDate *theDate = [thisLocation valueForKey:@"timestamp"];
                dataBytes[i] = [theDate timeIntervalSinceReferenceDate];
            }
            else if (dataFieldName == kGPSLatitude)
                dataBytes[i] = [thisLocation coordinate].latitude;
            else if (dataFieldName == kGPSLongitude)
                dataBytes[i] = [thisLocation coordinate].longitude;
            else
            {
                NSString *dataName = [self getDataNameFromKey:dataFieldName];
                dataBytes[i] = [[thisLocation valueForKey:dataName] doubleValue];
            }
            
        }
        [returnData appendData:[NSData dataWithBytes:dataBytes length:sizeof(double)*self.measurements.count]];
        free(dataBytes);
    }
    

    return returnData;
    
}

@end

/*
 
 @synthesize nBytesInAltitude = _nBytesInAltitude;
 @synthesize nBytesInCourse = _nBytesInCourse;
 @synthesize nBytesInHorizontalAccuracy = _nBytesInHorizontalAccuracy;
 @synthesize nBytesInLatitude = _nBytesInLatitude;
 @synthesize nBytesInLongitude = _nBytesInLongitude;
 @synthesize nBytesInSpeed = _nBytesInSpeed;
 @synthesize nBytesInVerticalAccuracy = _nBytesInVerticalAccuracy;

 
 -(int) nbytesInDataField:(NSString*)dataField
 {
 int length = [super nbytesInDataField:dataField];
 
 if ([dataField isEqualToString:@"altitude"])
 length = [self nBytesInAltitude];
 if ([dataField isEqualToString:@"course"])
 length = [self nBytesInCourse];
 if ([dataField isEqualToString:@"horizontalAccuracy"])
 length = [self nBytesInHorizontalAccuracy];
 if ([dataField isEqualToString:@"latitude"])
 length = [self nBytesInLatitude];
 if ([dataField isEqualToString:@"longitude"])
 length = [self nBytesInLongitude];
 if ([dataField isEqualToString:@"speed"])
 length = [self nBytesInSpeed];
 if ([dataField isEqualToString:@"verticalAccuracy"])
 length = [self nBytesInVerticalAccuracy];
 
 return length;
 
 }
 -(int) nBytesInData
 {
 return [super nBytesInData] + _nBytesInAltitude + _nBytesInCourse + _nBytesInHorizontalAccuracy + _nBytesInLatitude + _nBytesInLongitude + _nBytesInSpeed + _nBytesInVerticalAccuracy;
 }
 -(int) nBytesInLatitude
 {
 return self.nPoints*sizeof(CLLocationDegrees);
 }
 -(int) nBytesInLongitude
 {
 return self.nPoints*sizeof(CLLocationDegrees);    
 }
 -(int) nBytesInAltitude
 {
 return self.nPoints*sizeof(CLLocationDistance);
 }
 -(int) nBytesInSpeed
 {
 return self.nPoints*sizeof(CLLocationSpeed);
 }
 -(int) nBytesInCourse
 {
 return self.nPoints*sizeof(CLLocationDirection);
 }
 -(int) nBytesInHorizontalAccuracy
 {
 return self.nPoints*sizeof(CLLocationAccuracy);
 }
 -(int) nBytesInVerticalAccuracy
 {
 return self.nPoints*sizeof(CLLocationAccuracy);    
 }
*/