//
//  LocationDataCollector.h
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataCollector.h"
#import <CoreLocation/CoreLocation.h>

@interface LocationDataCollector : DataCollector

-(void)doAddLocationToData:(CLLocation*)newLocation;
-(void)doAddHeadingToData:(CLHeading*)newHeading;

@property (nonatomic,strong) NSMutableArray* time;
@property (nonatomic,strong) NSMutableArray* measurements;
@end


/*
-(NSTimeInterval)timeAtIndex:(int)i;
*/
