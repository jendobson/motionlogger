//
//  MagnetometerDataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MagnetometerDataCollector.h"
#import "CollectionEvent.h"
#import "DeviceManager.h"


@implementation MagnetometerDataCollector

-(NSString*)sensorType
{
return @"Magnetometer";
}

-(NSString*)getDataNameFromKey:(EventDataName)dataNameKey
{
    NSString* dataName;
    
    if (dataNameKey == kMagnetometerTime)
        dataName = @"time";
    if (dataNameKey == kMagnetometerXData)
        dataName = @"xData";
    if (dataNameKey == kMagnetometerYData)
        dataName = @"yData";
    if (dataNameKey == kMagnetometerZData)
        dataName = @"zData";
    
    return dataName;
}
-(void) doStartDataCollectionInEvent:(CollectionEvent *)event
{
    // Call the superclass method
    [super doStartDataCollectionInEvent:event];
    
    __weak MotionDataCollector *weakSelf = self;
    
   
    if (event.deviceManager.motionManager.magnetometerAvailable)
        [event.deviceManager.motionManager startMagnetometerUpdatesToQueue:event.opQ 
                                                                 withHandler:^(CMMagnetometerData *data, NSError *error)
         {
             // 25-Dec-2012  Fix for dereferencing a weak pointer not allowed:  make strong variable *internalSelf
             MotionDataCollector *internalSelf = weakSelf;
             if (*internalSelf->count < self.maxArraySize)
             {
                 internalSelf.xData[*internalSelf->count] = data.magneticField.x;
                 internalSelf.yData[*internalSelf->count] = data.magneticField.y;
                 internalSelf.zData[*internalSelf->count] = data.magneticField.z;
                 internalSelf.time[*internalSelf->count] = data.timestamp;
                 
                 (*internalSelf->count)++;
                 
             }
         }
         ];
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Magnetometer is not available."
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void) doStopDataCollectionInEvent:(CollectionEvent *)event
{
    [event.deviceManager.motionManager stopMagnetometerUpdates];
    [super doStopDataCollectionInEvent:event];
    
}
@end
