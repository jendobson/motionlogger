//
//  MotionDataCollector.h
//  DataLogger
//
//  Created by Jennifer Dobson on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CMMotionManager.h>
#import "DataCollector.h"

@interface MotionDataCollector : DataCollector
{
    int x, *count;
}

@property (nonatomic) int maxArraySize;

@property (nonatomic) double *xData;
@property (nonatomic) double *yData;
@property (nonatomic) double *zData;
@property (nonatomic) double *time;
@property (nonatomic) NSTimeInterval updateInterval;

@end
