//
//  DebugUtilities.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebugUtilities : NSObject

+(void)displayNSDataAsDouble:(NSData*)theData;

@end
