//
//  CollectionEvent.m
//  DataLogger
//
//  Created by Jennifer Dobson on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CollectionEvent.h"
#import "DeviceManager.h"
#import "GyroscopeDataCollector.h"
#import "HeadingDataCollector.h"
#import "GPSDataCollector.h"
#import "AccelerometerDataCollector.h"
#import "MagnetometerDataCollector.h"
#import "SettingsModel.h"
#import "SetGPSAccuracyViewController.h"

#import "AppDelegate.h"
#import "Event+Create.m"

@interface CollectionEvent ()
{
@private
}
-(void)resetDate;
@end

@implementation CollectionEvent

@synthesize persistentEvent = _persistentEvent;

@synthesize startTime = _startTime;
@synthesize stopTime = _stopTime;
@synthesize upTimeAtStart = _upTimeAtStart;

@synthesize deviceManager = _deviceManager;
@synthesize timer = _timer;
@synthesize isCollectingData = _isCollectingData;
@synthesize userMarks = _userMarks;

@synthesize name = _name;
@synthesize isWritingData = _isWritingData;

CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(CollectionEvent)

#pragma mark - init/dealloc


- (id) init
{
    self = [super init];
    if (self)
    {

        self.deviceManager = [[DeviceManager alloc] init];
        self.userMarks = [NSMutableArray array];
        [self reset];
        self.isWritingData = NO;
        
}
    
    return self;
}


#pragma mark - SET/GET methods
-(void)setStartTime:(NSDate *)startTime
{
    _startTime = startTime;
    _upTimeAtStart = [[NSProcessInfo processInfo] systemUptime];
}

-(NSOperationQueue*)opQ
{
    return self.deviceManager.opQ;
}
-(NSString*)gpsAccuracyString
{
    return [SetGPSAccuracyViewController gpsAccuracyStringForLocationAccuracy:self.gpsAccuracy];
}

-(NSTimeInterval)timeOutInterval
{
    return [[SettingsModel sharedSettingsModel] timeOutInterval];
}
-(void)setTimeOutInterval:(NSTimeInterval)timeOutInterval
{
    [[SettingsModel sharedSettingsModel] setTimeOutInterval:timeOutInterval];
}

-(void)setGpsAccuracy:(CLLocationAccuracy)gpsAccuracy
{
    self.deviceManager.locationManager.desiredAccuracy = gpsAccuracy; 
  }
-(CLLocationAccuracy)gpsAccuracy
{
    return self.deviceManager.gpsLocationAccuracy;
}
-(void)setCollectsGPSData:(BOOL)collectsGPSData
{
    self.deviceManager.collectsGPSData = collectsGPSData;
}
-(BOOL)collectsGPSData
{
    return self.deviceManager.collectsGPSData;
}
-(void)setCollectsHeadingData:(BOOL)collectsHeadingData
{
    self.deviceManager.collectsHeadingData = collectsHeadingData;
}
-(BOOL)collectsHeadingData
{
    return self.deviceManager.collectsHeadingData;
}
-(void)setCollectsAccelerometerData:(BOOL)collectsAccelerometerData
{
    self.deviceManager.collectsAccelerometerData = collectsAccelerometerData;
}
-(BOOL)collectsAccelerometerData
{
    return self.deviceManager.collectsAccelerometerData;
}
-(void)setCollectsGyroscopeData:(BOOL)collectsGyroscopeData
{
    self.deviceManager.collectsGyroscopeData = collectsGyroscopeData;
}
-(BOOL)collectsGyroscopeData
{
    return self.deviceManager.collectsGyroscopeData;
}
-(void)setCollectsMagnetometerData:(BOOL)collectsMagnetometerData
{
    self.deviceManager.collectsMagnetometerData = collectsMagnetometerData;
}
-(BOOL)collectsMagnetometerData
{
    return self.deviceManager.collectsMagnetometerData;
}
-(void)setSamplingFrequency:(double)samplingFrequency
{
    
    self.deviceManager.samplingFrequency = samplingFrequency;
}
-(double)samplingFrequency
{
    return self.deviceManager.samplingFrequency;
}

-(NSDictionary*)currentReading
{
    NSArray *keys = [NSArray arrayWithObjects:@"Latitude",@"Longitude",@"Altitude",@"Speed",@"Course",@"Heading",@"Gyroscope",@"Magnetometer",@"Accelerometer", nil];
    NSNumber *defNum = [NSNumber numberWithInt:-1];
    NSArray *objects = [NSArray arrayWithObjects:defNum,defNum,defNum,defNum,defNum,defNum,defNum,defNum,defNum,nil];
                        
    NSDictionary *reading = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    return reading;
    
}

-(NSDictionary*)statistics
{
    NSArray *keys = [NSArray arrayWithObjects:@"MeanHeading",@"MeanAcceleration",@"MeanGyroscope",@"MeanMagnetometer", nil];
    NSNumber *defNum = [NSNumber numberWithInt:-1];
    NSArray *objects = [NSArray arrayWithObjects:defNum,defNum,defNum,defNum, nil];
    
    NSDictionary *meanValues = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    return meanValues;

}

# pragma mark - methods
-(void)reset
{
    
    self.persistentEvent = nil;
    
    [self resetDate];
    [self.deviceManager reset];
    
   NSDate *initDate = [NSDate date];
    
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:initDate];
    
    _name = [NSString stringWithFormat:@"Event_%4i%02i%02iT%02i%02i%02i",[dateComponents year],[dateComponents month],[dateComponents day],[dateComponents hour],[dateComponents minute],[dateComponents second]];

}
-(void) resetDate
{
    self.startTime = [NSDate distantPast];
    self.stopTime = [NSDate distantFuture];
}

-(void) startCollection
{

     // Turn on screen lock
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    
    
    // TURN OFF Collection if devices not available
    if (![CLLocationManager locationServicesEnabled])
        self.collectsGPSData = NO;
    
    if (![CLLocationManager headingAvailable])
        self.collectsHeadingData = NO;
    
    if (!self.deviceManager.motionManager.gyroAvailable)
        self.collectsGyroscopeData = NO;
    
    if (!self.deviceManager.motionManager.accelerometerAvailable)
        self.collectsAccelerometerData = NO;
    
    if (!self.deviceManager.motionManager.magnetometerAvailable)
        self.collectsMagnetometerData = NO;
    

    [self reset];
    
    [SettingsModel sharedSettingsModel].writable = NO;
    
    
    self.isCollectingData = YES;
    
    [self.deviceManager startDataCollectionInEvent:self];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timeOutInterval target:self selector:@selector(respondToCollectionTimer:) userInfo:nil repeats:NO];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data Collection Started" object:self];


}

-(void) stopCollection
{
    if (!self.isCollectingData)
        return;
    
    self.isCollectingData = NO;
    
    // Invalidate the timer
    if (nil!=self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }

    self.stopTime = [NSDate date];
    
    [self.deviceManager stopDataCollectionInEvent:self];
    
    // Turn off screen lock
    UIApplication *application = [UIApplication sharedApplication];
    application.idleTimerDisabled = NO;

    // Post notification of data collection stopped
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Data Started Writing" object:self];
    
    NSDate *runUntil = [NSDate dateWithTimeIntervalSinceNow: .01 ];
    [[NSRunLoop currentRunLoop] runUntilDate:runUntil];
    
    
    __weak CollectionEvent* weakSelf = self;
    
    bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        
        weakSelf.isWritingData = NO;
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
        
    }];
    
    
    // Start the data writing
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        weakSelf.isWritingData = YES;
        // Write collection event to core data
        NSManagedObjectContext *appcontext = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
        self.persistentEvent = [Event eventFromCollectionEvent:self inManagedObjectContext:appcontext];
        
        // Reenable settings to be written to
        [SettingsModel sharedSettingsModel].writable = YES;
        
        // Need to post notification of data collection stopped on main thread
        NSNotification* notification = [NSNotification notificationWithName:@"Data Finished Writing" object:self];
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:notification waitUntilDone:YES];
        
        weakSelf.isWritingData = NO;
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    });

   

    
    }

-(void) addUserMark:(UserMark *)mark
{
    [self.userMarks addObject:mark];
}

+(DataCollector*)dataCollectorForEventDataName:(EventDataName)eventDataName andEvent:(CollectionEvent*)event
{
    DataCollector* dataCollector;
    switch (eventDataName) {
        case kGyroTime:
        case kGyroXData:
        case kGyroYData:
        case kGyroZData:
            dataCollector = event.deviceManager.gyroscopeDataCollector;
            break;
        case kAccelerometerTime:
        case kAccelerometerXData:
        case kAccelerometerYData:
        case kAccelerometerZData:
            dataCollector = event.deviceManager.accelerometerDataCollector;
            break;
        case kGPSAltitude:
        case kGPSCourse:
        case kGPSTime:
        case kGPSSpeed:
        case kGPSLatitude:
        case kGPSLongitude:
        case kGPSTimeStamp:
        case kGPSVerticalAccuracy:
        case kGPSHorizontalAccuracy:
            dataCollector = event.deviceManager.gpsDataCollector;
            break;
        case kHeadingX:
        case kHeadingY:
        case kHeadingZ:
        case kHeadingTime:
        case kHeadingTimeStamp:
        case kHeadingTrueHeading:
        case kHeadingHeadingAccuracy:
        case kHeadingMagneticHeading:
            dataCollector = event.deviceManager.headingDataCollector;
            break;
        case kMagnetometerTime:
        case kMagnetometerXData:
        case kMagnetometerYData:
        case kMagnetometerZData:
            dataCollector = event.deviceManager.magnetometerDataCollector;
            break;
        default:
            break;
    }
    return dataCollector;
}


-(NSData*) getEventData:(EventDataName)eventDataName
{
    return [CollectionEvent getEventData:eventDataName forEvent:self];
}


+(NSData*) getEventData:(EventDataName)eventDataName forEvent:(CollectionEvent*)event
{
    NSData *eventData = [NSData data];
    
    eventData = [[CollectionEvent dataCollectorForEventDataName:eventDataName andEvent:event] bytesForDataField:eventDataName];
    
    return eventData;
}

-(NSNumber*)getNPoints:(EventDataName)eventDataName
{
    int npoints;
    npoints = [[CollectionEvent dataCollectorForEventDataName:eventDataName andEvent:self] nPoints];
    return [NSNumber numberWithInt:npoints];
    
}

#pragma mark - Callbacks
-(void) respondToCollectionTimer:(NSTimer *)theTimer
{
    // Invalidate the timer
    [self.timer invalidate];
    self.timer = nil;
    
    [self stopCollection];

}



@end

/* 
 Event Post-process methods
 
 -(void) writeToCSV
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];
 }
 -(void) writeToMAT
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];  
 }
 -(void) writeToGPX
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];
 }
 -(void) writeToKML
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];
 }
 -(void) deleteEvent
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];
 }
 -(void) setGPSAccuracy
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];
 }
 -(void) emailEvent
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];
 }
 -(void) showEventOnMap
 {
 @throw [NSException exceptionWithName:NSInternalInconsistencyException
 reason:[NSString stringWithFormat:@"%@ is not implemented", NSStringFromSelector(_cmd)]
 userInfo:nil];
 }

*/