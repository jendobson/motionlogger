//
//  DeviceManager.h
//  DataLogger
//
//  Created by Jennifer Dobson on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreMotion/CMMotionManager.h>
#import <CoreLocation/CoreLocation.h>


@class CollectionEvent,LocationManagerDelegate;
@class GPSDataCollector, HeadingDataCollector, MagnetometerDataCollector, AccelerometerDataCollector, GyroscopeDataCollector;

@interface DeviceManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic) BOOL collectsGyroscopeData;
@property (nonatomic) BOOL collectsMagnetometerData;
@property (nonatomic) BOOL collectsAccelerometerData;
@property (nonatomic) BOOL collectsGPSData;
@property (nonatomic) BOOL collectsHeadingData;

@property (nonatomic, readonly) GPSDataCollector *gpsDataCollector;
@property (nonatomic, readonly) HeadingDataCollector *headingDataCollector;
@property (nonatomic, readonly) AccelerometerDataCollector *accelerometerDataCollector;
@property (nonatomic, readonly) MagnetometerDataCollector *magnetometerDataCollector;
@property (nonatomic, readonly) GyroscopeDataCollector *gyroscopeDataCollector;

@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic) double samplingFrequency;
@property (nonatomic) CLLocationAccuracy gpsLocationAccuracy;


@property (nonatomic, strong) NSOperationQueue *opQ;


-(void) startDataCollectionInEvent:(CollectionEvent*)event;
-(void) stopDataCollectionInEvent:(CollectionEvent*)event;
-(void) reset;
@end
