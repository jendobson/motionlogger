//
//  AppDelegate.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 4/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AcquireDataViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    __block UIBackgroundTaskIdentifier bgTask;

}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (weak, nonatomic) AcquireDataViewController* advc;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
