//
//  AcquireDataViewController.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Event;

@interface AcquireDataViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *timeSinceStartLabel;

@property (weak, nonatomic) IBOutlet UILabel *gpsNPointsLabel;

@property (weak, nonatomic) IBOutlet UILabel *headingNPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *accelerometerNPointsLabel;

@property (weak, nonatomic) IBOutlet UILabel *magnetometerNPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *gyroscopeNPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeOutInTimeLabel;

@property (weak, nonatomic) IBOutlet UIButton *startStopButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *collectionActivityIndicator;

@property (strong, nonatomic) NSTimer* updateTimer;

- (IBAction)startStopButtonPressed:(UIButton *)sender;
-(void) prepareForBackground;
-(void) prepareForForeground;
@end
