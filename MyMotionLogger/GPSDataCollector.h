//
//  GPSDataCollector.h
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationDataCollector.h"

@interface GPSDataCollector : LocationDataCollector

@end



/*
 @property (nonatomic,readonly) int nBytesInLatitude;
 @property (nonatomic,readonly) int nBytesInLongitude;
 @property (nonatomic,readonly) int nBytesInAltitude;
 @property (nonatomic,readonly) int nBytesInSpeed;
 @property (nonatomic,readonly) int nBytesInCourse;
 @property (nonatomic,readonly) int nBytesInHorizontalAccuracy;
 @property (nonatomic,readonly) int nBytesInVerticalAccuracy;
 */
