//
//  Event.m
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 10/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic gpsDesiredAccuracy;
@dynamic nPointsAccelerometer;
@dynamic nPointsGPS;
@dynamic nPointsGyroscope;
@dynamic nPointsHeading;
@dynamic nPointsMagnetometer;
@dynamic samplingFrequency;
@dynamic startTime;
@dynamic stopTime;
@dynamic userNotes;

@end
