//
//  EventSummaryViewController.m
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventSummaryViewController.h"
#import "Event.h"
#import "Event+Create.h"
#import "AppDelegate.h"


@interface EventSummaryViewController ()

@end

@implementation EventSummaryViewController

@synthesize event = _event;

@synthesize startTimeStampTextLabel;
@synthesize userNotesTextLabel;
@synthesize timeElapsedLabel;
@synthesize fileSizeLabel;
@synthesize gpsDataPointsLabel;
@synthesize headingDataPointsLabel;
@synthesize accelerometerDataPointsLabel;
@synthesize magnetometerDataPointsLabel;
@synthesize gyroscopeDataPointsLabel;
@synthesize theView;
@synthesize theScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithEvent:(Event*)event
{
    self = [self initWithNibName:@"EventSummaryViewController" bundle:nil];
    
    if (self)
        self.event = event;

    return self;
}


- (void)viewDidLoad
{
    self.navigationItem.title = @"Data Summary";
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDateFormatter *dateFormatter = [Event getDateFormatterForReadableText];
    
    self.startTimeStampTextLabel.text = [dateFormatter stringFromDate:self.event.startTime];
    
   
    int timeElapsedRawSecs = (int)[self.event.stopTime timeIntervalSinceDate:self.event.startTime];
    
    int timeElapsedHours = floor(timeElapsedRawSecs/3600);
    int timeElapsedMins = floor((timeElapsedRawSecs - timeElapsedHours*3600)/60);
    int timeElapsedSeconds = timeElapsedRawSecs - 3600*timeElapsedHours - 60*timeElapsedMins;
    
    self.timeElapsedLabel.text = [NSString stringWithFormat:@"%02i:%02i:%02i",timeElapsedHours,timeElapsedMins,timeElapsedSeconds];
    
    self.userNotesTextLabel.delegate = self;
    self.userNotesTextLabel.text = self.event.userNotes;
    
    NSString *accLabel = @"---";
    if (![self.event.nPointsAccelerometer isEqualToNumber:[NSNumber numberWithInt:0]])
        accLabel = [self.event.nPointsAccelerometer stringValue];
    self.accelerometerDataPointsLabel.text = accLabel;
    
    NSString *gpsLabel = @"---";
    if (![self.event.nPointsGPS isEqualToNumber:[NSNumber numberWithInt:0]])
        gpsLabel = [self.event.nPointsGPS stringValue];
    self.gpsDataPointsLabel.text = gpsLabel;
    
    NSString *gyroLabel = @"---";
    if (![self.event.nPointsGyroscope isEqualToNumber:[NSNumber numberWithInt:0]])        gyroLabel = [self.event.nPointsGyroscope stringValue];
    self.gyroscopeDataPointsLabel.text = gyroLabel;
    
    NSString *headingLabel = @"---";
    if (![self.event.nPointsHeading isEqualToNumber:[NSNumber numberWithInt:0]])
        headingLabel = [self.event.nPointsHeading stringValue];
    self.headingDataPointsLabel.text = headingLabel;
    
    NSString *magLabel = @"---";
    if (![self.event.nPointsMagnetometer isEqualToNumber:[NSNumber numberWithInt:0]])
        magLabel = [self.event.nPointsMagnetometer stringValue];
    self.magnetometerDataPointsLabel.text = magLabel;
    
    // Get approximate file sizes
    NSFileManager *manager = [NSFileManager defaultManager];
    NSDictionary *gpsAttributes = [manager attributesOfItemAtPath:[self.event.gpsFileURL path] error:nil];
    NSDictionary *headingAttributes = [manager attributesOfItemAtPath:[self.event.headingFileURL path] error:nil];
    NSDictionary *magnetometerAttributes = [manager attributesOfItemAtPath:[self.event.magnetometerFileURL path] error:nil];
    NSDictionary *accelerometerAttributes = [manager attributesOfItemAtPath:[self.event.accelerometerFileURL path] error:nil];
    NSDictionary *gyroscopeAttributes = [manager attributesOfItemAtPath:[self.event.gyroscopeFileURL path] error:nil];
    
    int totalSize = [[gpsAttributes objectForKey:NSFileSize] intValue] + [[headingAttributes objectForKey:NSFileSize] intValue] + [[magnetometerAttributes objectForKey:NSFileSize] intValue] + [[accelerometerAttributes objectForKey:NSFileSize] intValue] + [[gyroscopeAttributes objectForKey:NSFileSize] intValue];
    
    
    if (totalSize < 1000000)
        self.fileSizeLabel.text = [NSString stringWithFormat:@"(%i KB)",(int)round(totalSize/1000)];
    else {
        self.fileSizeLabel.text = [NSString stringWithFormat:@"(%i MB)",(int)round(totalSize/1000000)];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollUserNotesUp:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollUserNotesDown:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"viewWillAppear... checking to see if data is still here");
    
    // Check to see whether the event is still in the database
    // Get the events that have been logged
    NSManagedObjectContext *appcontext = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
    request.predicate = [NSPredicate predicateWithFormat:@"startTime = %@",self.event.startTime];
       
    NSError *error;
    NSArray *fetchedEvents = [appcontext executeFetchRequest:request error:&error];
    
    
    //NSLog(@"fetchedEvents: %@",fetchedEvents);
    
    if (fetchedEvents.count == 0)
        [self.navigationController popViewControllerAnimated:YES];
        
}

- (void)viewDidUnload
{
    [self setStartTimeStampTextLabel:nil];
    [self setUserNotesTextLabel:nil];
    [self setTheView:nil];
    [self setTheScrollView:nil];
    [self setGpsDataPointsLabel:nil];
    [self setHeadingDataPointsLabel:nil];
    [self setAccelerometerDataPointsLabel:nil];
    [self setMagnetometerDataPointsLabel:nil];
    [self setGyroscopeDataPointsLabel:nil];
    [self setTimeElapsedLabel:nil];
    [self setFileSizeLabel:nil];
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)emailCSVFile:(id)sender {
    
    
     
    NSDateFormatter *formatter = [Event getDateFormatterForReadableText];

    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setSubject:[NSString stringWithFormat:@"%@ Data Collection",[formatter stringFromDate:self.event.startTime]]];

    NSDateFormatter *fileFormatter = [Event getDateFormatterForFilenames];
                                                                   
    
     NSData* gpsFileData = [NSData dataWithContentsOfURL:self.event.gpsFileURL];
    if (nil!=gpsFileData)
        [picker addAttachmentData:gpsFileData mimeType:@"text/plain" fileName:[NSString stringWithFormat:@"%@_gps.csv",[fileFormatter stringFromDate:self.event.startTime]]];
     
     NSData* headingFileData = [NSData dataWithContentsOfURL:self.event.headingFileURL];
    if (nil!=headingFileData)
        [picker addAttachmentData:headingFileData mimeType:@"text/plain" fileName:[NSString stringWithFormat:@"%@_heading.csv",[fileFormatter stringFromDate:self.event.startTime]]];

    
    NSData* accelerometerFileData = [NSData dataWithContentsOfURL:self.event.accelerometerFileURL];
    if (nil!= accelerometerFileData)
        [picker addAttachmentData:accelerometerFileData mimeType:@"text/plain" fileName:[NSString stringWithFormat:@"%@_accelerometer.csv",[fileFormatter stringFromDate:self.event.startTime]]];

  
    NSData* gyroscopeFileData = [NSData dataWithContentsOfURL:self.event.gyroscopeFileURL];
    
    if (nil!=gyroscopeFileData)
        [picker addAttachmentData:gyroscopeFileData mimeType:@"text/plain" fileName:[NSString stringWithFormat:@"%@_gyroscope.csv",[fileFormatter stringFromDate:self.event.startTime]]];

    
    NSData* magnetometerFileData = [NSData dataWithContentsOfURL:self.event.magnetometerFileURL];
    if (nil!=magnetometerFileData)
        [picker addAttachmentData:magnetometerFileData mimeType:@"text/plain" fileName:[NSString stringWithFormat:@"%@_magnetometer.csv",[fileFormatter stringFromDate:self.event.startTime]]];

    
    /*
     // Set up the recipients.
     NSArray *toRecipients = [NSArray arrayWithObjects:[[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultEmailAddress],
     nil];
     
     [picker setToRecipients:toRecipients];
     */
    
    NSString *messageBody = self.event.userNotes;    
     [picker setMessageBody:messageBody isHTML:NO];
     
     // Present the mail composition interface.
     [self presentModalViewController:picker animated:YES];

    //[self.event deleteSecondaryFiles];
    
}
  
// The mail compose view controller delegate method
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UITextViewDelegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"]) {
        [textView resignFirstResponder];
        self.event.userNotes = textView.text;
        return NO;
    }
    return YES;
}

-(void)scrollUserNotesUp:(NSNotification*)notification
{
    //NSLog(@"notification: %@",notification);
    
    NSValue* keyboardFrame = [notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameRect = CGRectMake(0, 0, 0, 0);
    [keyboardFrame getValue:&keyboardFrameRect];
    self.theScrollView.contentOffset = CGPointMake(0,CGRectGetHeight(keyboardFrameRect));
}

-(void)scrollUserNotesDown:(NSNotification*)notification
{
    //NSLog(@"notification: %@",notification);
    self.theScrollView.contentOffset = CGPointMake(0,0);
    
}
@end
