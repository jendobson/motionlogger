//
//  Event+Create.m
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Event+Create.h"

#import "CollectionEvent.h"

#import "DebugUtilities.h"



@implementation Event (Create)

+ (NSDateFormatter*)getDateFormatterForReadableText
{
    static NSDateFormatter* eventDateFormatter;
    
    if (nil==eventDateFormatter)
    {
        eventDateFormatter = [[NSDateFormatter alloc] init];
        [eventDateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [eventDateFormatter setTimeStyle:NSDateFormatterMediumStyle];
        
    }
    return eventDateFormatter;
}


-(NSURL*)eventDataURL
{
    
    // Data directory will have name as yyyyMMddTHHmmss of event collection start time.
    // In this directory there will be three files: "gps.csv", "heading.csv", and "motion.csv", which contain the gps, heading, and motion sensor data, respectively.
    
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSURL *documentsDirectory = [manager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDirectory].lastObject;
    
    
    NSString* subDirName = [[Event getDateFormatterForFilenames] stringFromDate:self.startTime];
    
    NSURL *eventDataURL = [documentsDirectory URLByAppendingPathComponent:subDirName isDirectory:YES];
    
    return eventDataURL;
    
}

-(NSURL*)gpsFileURL
{
    // ---Location File
    NSURL *gpsFileURL = [self.eventDataURL URLByAppendingPathComponent:@"gps.csv" isDirectory:NO];
    return gpsFileURL;
}

-(NSURL*)headingFileURL
{
    // ---Heading File
    return [self.eventDataURL URLByAppendingPathComponent:@"heading.csv" isDirectory:NO];    
}

-(NSURL*)gyroscopeFileURL
{
    // ---Motion Data File
    return [self.eventDataURL URLByAppendingPathComponent:@"gyroscope.csv" isDirectory:NO];
}
-(NSURL*)magnetometerFileURL
{
    // ---Motion Data File
    return [self.eventDataURL URLByAppendingPathComponent:@"magnetometer.csv" isDirectory:NO];
}
-(NSURL*)accelerometerFileURL
{
    // ---Motion Data File
    return [self.eventDataURL URLByAppendingPathComponent:@"accelerometer.csv" isDirectory:NO];
}

+ (NSDateFormatter*)getDateFormatterForFilenames
{
    static NSDateFormatter* eventDateFormatter;
    
    if (nil==eventDateFormatter)
    {
        eventDateFormatter = [[NSDateFormatter alloc] init];
        [eventDateFormatter setDateFormat:@"yyyyMMdd'T'HHmmss"];
    }
    return eventDateFormatter;
}


-(void)writeDataStringWithXData:(NSData*)xData 
                          YData:(NSData*)yData
                          ZData:(NSData*)zData
                           Time:(NSData*)timeData
                          Units:(NSString*)unitsString
                        nPoints:(int)nPoints
                        fileURL:(NSURL*)fileURL
                       zeroTime:(double)zeroTime

{
    NSMutableString* string = [NSMutableString stringWithFormat:@"x (%@),y (%@),z (%@),time (s),\n",unitsString,unitsString,unitsString];

    double *x = (double*)[xData bytes];
    
    double *y = (double*)[yData bytes];
    double *z = (double*)[zData bytes];
    double *time = (double*)[timeData bytes];
    
    
    // Create a string of location data
    for (int k = 0; k<nPoints;k++)
    {
        
        [string appendString:[NSString stringWithFormat:@"%lf,%lf,%lf,%lf,\n",x[k],y[k],z[k],time[k]-zeroTime]];
    }
    
    // Create the file
    NSError *error;
    // Write to the file
    [string writeToURL:fileURL atomically:YES encoding:NSUTF8StringEncoding error:&error];

}

-(void)writeMagnetometerDataForCollectionEvent:(CollectionEvent*)collectionEvent withZeroTime:(double)zeroTime
{
    NSData* xData = [collectionEvent getEventData:kMagnetometerXData];
    NSData* yData = [collectionEvent getEventData:kMagnetometerYData];
    NSData* zData = [collectionEvent getEventData:kMagnetometerZData];
    NSData* timeData = [collectionEvent getEventData:kMagnetometerTime];
    
    int nPoints = [self.nPointsMagnetometer intValue];
    
    NSURL* fileURL = self.magnetometerFileURL;
    
    NSString* unitsString = @"uT";
    [self writeDataStringWithXData:xData YData:yData ZData:zData Time:timeData Units:unitsString nPoints:nPoints fileURL:fileURL zeroTime:zeroTime];
                                
}
-(void)writeAccelerometerDataForCollectionEvent:(CollectionEvent*)collectionEvent withZeroTime:(double)zeroTime
{
    NSData *xData = [collectionEvent getEventData:kAccelerometerXData];
    NSData *yData = [collectionEvent getEventData:kAccelerometerYData];
    NSData *zData = [collectionEvent getEventData:kAccelerometerZData];
    NSData *timeData = [collectionEvent getEventData:kAccelerometerTime];
    
    int nPoints = [self.nPointsAccelerometer intValue];
    
    NSURL* fileURL = self.accelerometerFileURL;
    
    NSString* unitsString = @"g";
    
    [self writeDataStringWithXData:xData YData:yData ZData:zData Time:timeData Units:unitsString nPoints:nPoints fileURL:fileURL zeroTime:zeroTime];
    
    
}
-(void)writeGyroscopeDataForCollectionEvent:(CollectionEvent*)collectionEvent withZeroTime:(double)zeroTime
{
    NSData *xData = [collectionEvent getEventData:kGyroXData];
    NSData *yData = [collectionEvent getEventData:kGyroYData];
    NSData *zData = [collectionEvent getEventData:kGyroZData];
    NSData *timeData = [collectionEvent getEventData:kGyroTime];
    
    int nPoints = [self.nPointsGyroscope intValue];
    
    NSURL* fileURL = self.gyroscopeFileURL;
    
    NSString *unitsString = @"rad/sec";
    
    [self writeDataStringWithXData:xData YData:yData ZData:zData Time:timeData Units:unitsString nPoints:nPoints fileURL:fileURL zeroTime:zeroTime];
    
}

-(void) writeHeadingDataForCollectionEvent:(CollectionEvent*)collectionEvent withZeroTime:(double)zeroTime
{
    
    NSData* magneticHeadingData = [collectionEvent getEventData:kHeadingMagneticHeading];
    NSData* trueHeadingData = [collectionEvent getEventData:kHeadingTrueHeading];
    NSData* headingAccuracyData = [collectionEvent getEventData:kHeadingHeadingAccuracy];
    NSData* xData = [collectionEvent getEventData:kHeadingX];
    NSData* yData = [collectionEvent getEventData:kHeadingY];
    NSData* zData = [collectionEvent getEventData:kHeadingZ];
    NSData* timeData = [collectionEvent getEventData:kHeadingTime];
                  
    // Create the string
    NSMutableString *headingString = [NSMutableString stringWithString:@"magHeading (deg),trueHeading (deg),headAcc (deg),x (uT),y (uT),z (uT),time (s),\n"];
    
    double* magneticHeading = (double*)[magneticHeadingData bytes];
    
    double* trueHeading = (double*)[trueHeadingData bytes];
    
    double* headingAccuracy = (double*)[headingAccuracyData bytes];
    
    double* x = (double*)[xData bytes];
    
    double* y = (double*)[yData bytes];
    
    double* z = (double*)[zData bytes];
    
    double* time = (double*)[timeData bytes];
    
    
    // Create a string of heading data
    for (int k = 0; k<[self.nPointsHeading intValue];k++)
    {
        
        [headingString appendString:[NSString stringWithFormat:@"%lf,%lf,%lf,%lf,%lf,%lf,%lf,\n",magneticHeading[k],trueHeading[k],headingAccuracy[k],x[k],y[k],z[k],time[k]-zeroTime]];
    }
    
    
    // Create the file
    NSError *error;
    // Write to the file
    [headingString writeToURL:self.headingFileURL atomically:YES encoding:NSUTF8StringEncoding error:&error];
    NSLog(@"error: %@",error);
    
}
-(void)writeGPSDataForCollectionEvent:(CollectionEvent*)collectionEvent withZeroTime:(double)zeroTime
{
    
    NSData* latitude = [collectionEvent getEventData:kGPSLatitude];
    NSData* longitude = [collectionEvent getEventData:kGPSLongitude];
    NSData* time = [collectionEvent getEventData:kGPSTime];
    NSData* course = [collectionEvent getEventData:kGPSCourse];
    NSData* speed = [collectionEvent getEventData:kGPSSpeed];
    NSData* altitude = [collectionEvent getEventData:kGPSAltitude];
    NSData* verticalAccuracy = [collectionEvent getEventData:kGPSVerticalAccuracy];
    NSData* horizontalAccuracy = [collectionEvent getEventData:kGPSHorizontalAccuracy];
 
    // Create the string of data
    
    NSMutableString *gpsString = [NSMutableString stringWithString:@"lat (deg),lon (deg),haccuracy (m),alt (m),vaccuracy (m),speed (m/s),course (deg),time (s),\n"];
    double *latBytes = (double*)[latitude bytes];
    
    double *longBytes = (double*)[longitude bytes];
    
    double *haccBytes = (double*)[horizontalAccuracy bytes];
    
    double *vaccBytes = (double*)[verticalAccuracy bytes];
    
    double *altBytes = (double*)[altitude bytes];
    
    double *speedBytes = (double*)[speed bytes];
    
    double *courseBytes = (double*)[course bytes];
    
    double *timeBytes = (double*)[time bytes];
    
    // Create a string of location data
    for (int k = 0; k<[self.nPointsGPS intValue];k++)
    {
        
        [gpsString appendString:[NSString stringWithFormat:@"%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,\n",latBytes[k],longBytes[k],haccBytes[k],altBytes[k],vaccBytes[k],speedBytes[k],courseBytes[k],timeBytes[k]-zeroTime]];
    }
    
    // Create the file
    NSError *error;
    // Write to the file
    [gpsString writeToURL:self.gpsFileURL atomically:YES encoding:NSUTF8StringEncoding error:&error];
    NSLog(@"error: %@",error);
    
    
}





+ (Event *)eventFromCollectionEvent:(CollectionEvent *)collectionEvent
        inManagedObjectContext:(NSManagedObjectContext *)context
{
    //NSLog(@"Creating eventFromCollectionEvent");
    
    // Insert a new event in the database
    Event *event = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:context];
    
    event.startTime = collectionEvent.startTime;
    event.stopTime = collectionEvent.stopTime;
    event.samplingFrequency = [NSNumber numberWithDouble:collectionEvent.samplingFrequency];
    event.userNotes = [NSString stringWithFormat:@"Data collected on: %@.  Sampling Frequency = %.2f Hz.  GPS Accuracy = ""%@"".",[[Event getDateFormatterForReadableText] stringFromDate:event.startTime],collectionEvent.samplingFrequency,collectionEvent.gpsAccuracyString];
    
    
    double zeroTime = collectionEvent.upTimeAtStart;
    
    
    // Create a location for the data
    NSFileManager *manager = [NSFileManager defaultManager];
    
    if (![manager fileExistsAtPath:[event.eventDataURL path]])
    {
        NSError *createDirectoryError;
        [manager createDirectoryAtPath:[event.eventDataURL path] withIntermediateDirectories:YES attributes:nil error:&createDirectoryError];
    }
    

    // WRITE THE GYROSCOPE DATA
    if (collectionEvent.collectsGyroscopeData)
    {
        event.nPointsGyroscope = [collectionEvent getNPoints:kGyroTime];
        [event writeGyroscopeDataForCollectionEvent:collectionEvent withZeroTime:zeroTime];
        
    }
    
    // WRITE THE MAGNETOMETER DATA
    if (collectionEvent.collectsMagnetometerData)
    {
        event.nPointsMagnetometer = [collectionEvent getNPoints:kMagnetometerTime];
        [event writeMagnetometerDataForCollectionEvent:collectionEvent withZeroTime:zeroTime];
        }
    
    // WRITE THE ACCELEROMETER DATA
    if (collectionEvent.collectsAccelerometerData)
    {
        event.nPointsAccelerometer = [collectionEvent getNPoints:kAccelerometerTime];
        [event writeAccelerometerDataForCollectionEvent:collectionEvent withZeroTime:zeroTime];
   
    }
    
    // WRITE THE GPS DATA
    if (collectionEvent.collectsGPSData)
    {
        event.nPointsGPS = [collectionEvent getNPoints:kGPSTime];
        event.gpsDesiredAccuracy = collectionEvent.gpsAccuracyString;
        [event writeGPSDataForCollectionEvent:collectionEvent withZeroTime:zeroTime];
    }
    
    // WRITE THE HEADING DATA
    if (collectionEvent.collectsHeadingData)
    {
        event.nPointsHeading = [collectionEvent getNPoints:kHeadingTime];
        [event writeHeadingDataForCollectionEvent:collectionEvent withZeroTime:zeroTime];
    }
    
    NSError *error;
    [context save:&error];
    
    if (nil != error)
        NSLog(@"error on save: %@",error);
    
    collectionEvent.persistentEvent = event;
    
    return event;
    
}

-(void)deleteSecondaryFiles
{
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:[self.eventDataURL path]])
    {
        
        
        NSError* error;
        [manager removeItemAtURL:self.eventDataURL error:&error];
        
        
    }
}

-(void) prepareForDeletion
{
    //NSLog(@"event+create:prepareForDeletion");
    [self deleteSecondaryFiles];
}

@end
