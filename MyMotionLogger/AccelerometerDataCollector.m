//
//  AccelerometerDataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AccelerometerDataCollector.h"
#import "CollectionEvent.h"
#import "DeviceManager.h"

@implementation AccelerometerDataCollector

-(NSString*)sensorType
{
    return @"Accelerometer";
}

-(NSString*)getDataNameFromKey:(EventDataName)dataNameKey
{
    NSString* dataName;
    
    if (dataNameKey == kAccelerometerTime)
        dataName = @"time";
    if (dataNameKey == kAccelerometerXData)
        dataName = @"xData";
    if (dataNameKey == kAccelerometerYData)
        dataName = @"yData";
    if (dataNameKey == kAccelerometerZData)
        dataName = @"zData";
    
    return dataName;
}

-(void) doStartDataCollectionInEvent:(CollectionEvent *)event
{
    // Call the superclass method
    [super doStartDataCollectionInEvent:event];
    
    __weak MotionDataCollector *weakSelf = self;
  
    if (event.deviceManager.motionManager.accelerometerAvailable)
        [event.deviceManager.motionManager startAccelerometerUpdatesToQueue:event.opQ 
                                                        withHandler:^(CMAccelerometerData *data, NSError *error)
         {
             MotionDataCollector *internalSelf = weakSelf;
             if (*internalSelf->count < self.maxArraySize)
             {
                 internalSelf.xData[*internalSelf->count] = data.acceleration.x;
                 internalSelf.yData[*internalSelf->count] = data.acceleration.y;
                 internalSelf.zData[*internalSelf->count] = data.acceleration.z;
                 internalSelf.time[*internalSelf->count] = data.timestamp;
                 
                 (*internalSelf->count)++;
                 
             }
         }
         ];
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Accelerometer is not available."
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void) doStopDataCollectionInEvent:(CollectionEvent *)event
{
    [event.deviceManager.motionManager stopAccelerometerUpdates];
    [super doStopDataCollectionInEvent:event];
    
}

@end
