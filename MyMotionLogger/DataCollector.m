//
//  DataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataCollector.h"
#import "DeviceManager.h"
#import "CollectionEvent.h"

@implementation DataCollector

@synthesize nPoints = _nPoints;

#pragma mark - GET/SET methods
-(NSString *)sensorType
{
    return @"DataCollector";
}
-(int) nPoints
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];

}

-(int) nBytesInData
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}
-(int) nbytesInDataField:(NSString*)dataField
{
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}


-(void) startDataCollectionInEvent:(CollectionEvent*)event
{
    [self doStartDataCollectionInEvent:event];
    
}

-(void) stopDataCollectionInEvent:(CollectionEvent*)event
{
    [self doStopDataCollectionInEvent:event];
}

-(void) doStartDataCollectionInEvent:(CollectionEvent*)event
{
    [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass",NSStringFromSelector(_cmd)];
}

-(void) doStopDataCollectionInEvent:(CollectionEvent*)event
{
    [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass",NSStringFromSelector(_cmd)];

}

-(void)freeData
{
    [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass",NSStringFromSelector(_cmd)];
}

-(NSData*)bytesForDataField:(EventDataName)dataFieldName
{
    [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass",NSStringFromSelector(_cmd)];    
    return nil;
}

@end
