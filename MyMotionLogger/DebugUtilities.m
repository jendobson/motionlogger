//
//  DebugUtilities.m
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DebugUtilities.h"

@implementation DebugUtilities

+(void)displayNSDataAsDouble:(NSData*)theData

{
    int nDatum = theData.length/sizeof(double);
    NSLog(@"theData: %@",theData);
    
    double *theDataBuffer;
    theDataBuffer = (double*)[theData bytes];
    
    NSString* theString = [NSString string];
    
    for (int k = 0; k < nDatum; k++)
    {
        theString = [theString stringByAppendingFormat:@"%f\n",theDataBuffer[k]];
    }
    
    NSLog(@"%@",theString);
    
}

@end
