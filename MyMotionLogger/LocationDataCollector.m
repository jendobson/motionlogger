//
//  LocationDataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationDataCollector.h"
#import "DeviceManager.h"

@interface LocationDataCollector ()


@end

@implementation LocationDataCollector
@synthesize measurements = _measurements;
@synthesize time = _time;

#pragma mark - INIT
-(id) init
{
    self = [super init];
    if (self)
    {
        self.measurements = [NSMutableArray array];
        self.time = [NSMutableArray array];
    }
    
    return self;
}


#pragma mark - GET/SET methods
-(int) nPoints
{
    return (int) self.measurements.count;
}

-(NSString *)sensorType
{
    return @"Location";
}

-(void)freeData
{
    self.measurements = [NSMutableArray array];
    self.time = [NSMutableArray array];
}
#pragma mark - Subclass methods


-(void) doStartDataCollectionInEvent:(CollectionEvent*)event
{
     [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass",NSStringFromSelector(_cmd)];
}

-(void) doStopDataCollectionInEvent:(CollectionEvent*)event
{
     [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass",NSStringFromSelector(_cmd)];
}


- (void) doAddLocationToData:(CLLocation*)newLocation
{
    [NSException raise:NSInternalInconsistencyException 
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

- (void) doAddHeadingToData: (CLHeading*)newHeading
{
    [NSException raise:NSInternalInconsistencyException 
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}


@end

/*
 - (NSTimeInterval) timeAtIndex:(int)i
 {
 // Time since system restart
 return [[self.time objectAtIndex:i] doubleValue];
 }
 
 -(int) nBytesInTimeStamp
 {
 return sizeof(NSTimeInterval)*self.nPoints;
 }
 -(int) nBytesInTime
 {
 return self.time.count*sizeof(NSTimeInterval);
 }
 
 -(int) nbytesInDataField:(NSString *)dataField
 {
 int length = -1;
 
 if ([dataField isEqualToString:@"time"])
 length = [self nBytesInTime];
 if ([dataField isEqualToString:@"timestamp"])
 length = [self nBytesInTimeStamp];
 
 return length;
 }
*/