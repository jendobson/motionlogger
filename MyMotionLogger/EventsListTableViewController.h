//
//  EventsListTableViewController.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsListTableViewController : UITableViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (strong, nonatomic) NSArray* eventsArray;
- (IBAction)editButtonPressed:(id)sender;


@property (nonatomic) BOOL editState;


- (IBAction)deleteAllData:(id)sender;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;

@end
