//
//  MotionDataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MotionDataCollector.h"
#import "Constants.h"
#import "DeviceManager.h"
#import "CollectionEvent.h"

@interface MotionDataCollector ()

@property (nonatomic) int nBytesInXData;
@property (nonatomic) int nBytesInYData;
@property (nonatomic) int nBytesInZData;
@property (nonatomic) int nBytesInTimeData;

@end


@implementation MotionDataCollector


@synthesize xData = _xData;
@synthesize yData = _yData;
@synthesize zData = _zData;
@synthesize time = _time;

@synthesize nBytesInXData = _nBytesInXData;
@synthesize nBytesInYData = _nBytesInYData;
@synthesize nBytesInZData = _nBytesInZData;
@synthesize nBytesInTimeData = _nBytesInTimeData;

@synthesize updateInterval = _updateInterval;

@synthesize maxArraySize = _maxArraySize;


-(NSString*)getDataNameFromKey:(EventDataName)dataNameKey
{
    [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass",NSStringFromSelector(_cmd)];
    return nil;
}

#pragma mark - DataCollectionDelegateProtocol Methods

-(void) doStartDataCollectionInEvent:(CollectionEvent *)event;
{
    
    // Reset the data
    [self resetDataWithNSamples:(int)(event.samplingFrequency*event.timeOutInterval)];
    
   }
-(void) doStopDataCollectionInEvent:(CollectionEvent *)event
{
    
    /*
    NSString *dataString = [NSString string];
    
    for (int k = 0; k < self.nPoints; k++)
    {
        dataString = [dataString stringByAppendingFormat:@"%lf\n",self.xData[k]];
    }
    NSLog(@"Class: %@\ndataString: %@",[self class],dataString);
     */
}




# pragma mark - set/get methods

-(NSString*)sensorType
{
    return @"Motion";
}

# pragma mark - init/dealloc

- (id) init
{
    
    self = [super init];
    if (self)
    {
        
        // Make sure the data buffer is a NULL pointer
        self.xData = NULL;
        self.yData = NULL;
        self.zData = NULL;
        self.time = NULL;
        
        // Initialize update Interval
        self.updateInterval = 1/100;
        
        // Initialize data collection counter
        
        self->count = &self->x;
        *self->count = 0;
        
    }
    
    return self;
}

-(int)nPoints
{
    return *self->count;
}

-(void)dealloc
{
    [self freeData];
}

#pragma mark - Methods for getting number of bytes for data

#define member_size(type, member) sizeof(((type *)0)->member)
-(int)nBytesForXDataInitWithNSamples:(int)nSamps
{
    return nSamps*member_size(CMRotationRate, x);
}
-(int)nBytesForYDataInitWithNSamples:(int)nSamps
{
    return nSamps*member_size(CMRotationRate, y);
}
-(int)nBytesForZDataInitWithNSamples:(int)nSamps
{
    return nSamps*member_size(CMRotationRate, z);
}
-(int)nBytesForTimeInitWithNSamples:(int)nSamps
{
    return nSamps*sizeof(NSTimeInterval);
}

-(int)nBytesInXData
{
    return self.nPoints*member_size(CMRotationRate, x);
}
-(int)nBytesInYData
{
    return self.nPoints*member_size(CMRotationRate, y);
}
-(int)nBytesInZData
{
    return self.nPoints*member_size(CMRotationRate, z);
}
-(int)nBytesInTimeData
{
    return self.nPoints*sizeof(NSTimeInterval);
}


-(int) nbytesInDataField:(NSString*)dataField
{
    int length = -1;
    
    if ([dataField isEqualToString:@"xData"])
        length = [self nBytesInXData];
    if ([dataField isEqualToString:@"yData"])
        length = [self nBytesInYData];
    if ([dataField isEqualToString:@"zData"])
        length = [self nBytesInZData];
    if ([dataField isEqualToString:@"time"])
        length = [self nBytesInTimeData];
    
    return length;
    
}

#pragma mark - internal methods

-(void)resetDataWithNSamples:(int)nSamps
{
    // Free the current data if currently allocated
    [self freeData];
    
    [self doResetDataWithNSamples:nSamps];
    
    if (self.xData == NULL | self.yData == NULL | self.zData == NULL | self.time == NULL)
    {
        NSLog(@"ERROR: memory could not be allocated");
        [self freeData];
    }    
    
}

-(void)doResetDataWithNSamples:(int)nSamps
{
    // Allocate space for the new data, given the sampling frequency and time out interval
    self.xData = malloc ([self nBytesForXDataInitWithNSamples:nSamps]);
    self.yData = malloc ([self nBytesForYDataInitWithNSamples:nSamps]);
    self.zData = malloc ([self nBytesForZDataInitWithNSamples:nSamps]);
    self.time = malloc([self nBytesForTimeInitWithNSamples:nSamps]);
    self.maxArraySize = nSamps;
    
    
    // TODO: handle this error.  Need to kill start logging process and ask user to decrease the time out interval
}


-(void)freeData
{
    // Check whether the data is a NULL pointer.  If not, free it. 
    if (self.xData != NULL)
    {
        free(self.xData);
        self.xData = NULL;
    }
    
    
    if (self.yData != NULL)
    {
        free(self.yData);
        self.yData = NULL;
    }
    
    if (self.zData != NULL)
    {
        free(self.zData);
        self.zData = NULL;
    }
    
    if (self.time != NULL)
    {
        free(self.time);
        self.time = NULL;
    }
    
    // Reset data collection counter
    *self->count = 0;
    self.maxArraySize = 0;
    
}


-(NSData*)bytesForDataField:(EventDataName)dataNameKey
{
    NSData* dataBytes;
    NSString *dataName = [self getDataNameFromKey:dataNameKey];
    
    if ([dataName isEqualToString:@"time"])
        dataBytes = [NSData dataWithBytes:self.time length:[self nBytesInTimeData]];
        
    if ([dataName isEqualToString:@"xData"])
        dataBytes = [NSData dataWithBytes:self.xData length:[self nBytesInXData]];
        
    if ([dataName isEqualToString:@"yData"])
        dataBytes = [NSData dataWithBytes:self.yData length:[self nBytesInYData]];
        
    if ([dataName isEqualToString:@"zData"])
        dataBytes = [NSData dataWithBytes:self.zData length:[self nBytesInZData]];

    return dataBytes;
    
}


@end

