//
//  GyroscopeDataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GyroscopeDataCollector.h"
#import "CollectionEvent.h"
#import "DeviceManager.h"

@implementation GyroscopeDataCollector

-(NSString*)sensorType
{
    return @"Gyroscope";
}


-(NSString*)getDataNameFromKey:(EventDataName)dataNameKey
{
    NSString* dataName;
    
    if (dataNameKey == kGyroTime)
        dataName = @"time";
    if (dataNameKey == kGyroXData)
        dataName = @"xData";
    if (dataNameKey == kGyroYData)
        dataName = @"yData";
    if (dataNameKey == kGyroZData)
        dataName = @"zData";
    
    return dataName;
}
-(void) doStartDataCollectionInEvent:(CollectionEvent *)event
{
    
    [super doStartDataCollectionInEvent:event];

    
    __weak MotionDataCollector *weakSelf = self;
    
    
    if (event.deviceManager.motionManager.gyroAvailable)
        [event.deviceManager.motionManager startGyroUpdatesToQueue:event.opQ 
                                        withHandler:^(CMGyroData *data, NSError *error)
         {
             MotionDataCollector *internalSelf = weakSelf;
             if (*internalSelf->count < self.maxArraySize)
             {
                 internalSelf.xData[*internalSelf->count] = data.rotationRate.x;
                 internalSelf.yData[*internalSelf->count] = data.rotationRate.y;
                 internalSelf.zData[*internalSelf->count] = data.rotationRate.z;
                 internalSelf.time[*internalSelf->count] = data.timestamp;
                 
                 (*internalSelf->count)++;
             }
         }
         ];
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Gyroscope is not available."
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void) doStopDataCollectionInEvent:(CollectionEvent *)event
{
    [event.deviceManager.motionManager stopGyroUpdates];
    [super doStopDataCollectionInEvent:event];
    
}
@end
