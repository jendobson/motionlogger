//
//  EventSummaryViewController.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MFMailComposeViewController.h>

@class Event;

@interface EventSummaryViewController : UIViewController <MFMailComposeViewControllerDelegate,UITextViewDelegate>

@property (nonatomic, strong) Event* event;

// UI controls
@property (weak, nonatomic) IBOutlet UILabel *startTimeStampTextLabel;
@property (weak, nonatomic) IBOutlet UITextView *userNotesTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsedLabel;
@property (weak, nonatomic) IBOutlet UILabel *fileSizeLabel;


@property (weak, nonatomic) IBOutlet UILabel *gpsDataPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *headingDataPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *accelerometerDataPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *magnetometerDataPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *gyroscopeDataPointsLabel;

@property (weak, nonatomic) IBOutlet UIView *theView;
@property (weak, nonatomic) IBOutlet UIScrollView *theScrollView;


-(void)scrollUserNotesUp:(NSNotification*)notification;
-(void)scrollUserNotesDown:(NSNotification*)notification;

- (IBAction)emailCSVFile:(id)sender;



-(id)initWithEvent:(Event*)event;

@end
