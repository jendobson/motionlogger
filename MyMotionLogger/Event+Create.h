//
//  Event+Create.h
//  MyMotionLogger
//
//  Created by Jennifer Dobson on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Event.h"
@class CollectionEvent;

@interface Event (Create)

@property (readonly) NSURL* eventDataURL;
@property (readonly) NSURL* gyroscopeFileURL;
@property (readonly) NSURL* magnetometerFileURL;
@property (readonly) NSURL* accelerometerFileURL;
@property (readonly) NSURL* gpsFileURL;
@property (readonly) NSURL* headingFileURL;

+ (Event *)eventFromCollectionEvent:(CollectionEvent *)collectionEvent
             inManagedObjectContext:(NSManagedObjectContext *)context;

+ (NSDateFormatter*)getDateFormatterForFilenames;
+ (NSDateFormatter*)getDateFormatterForReadableText;

-(void)deleteSecondaryFiles;
-(void)prepareForDeletion;

@end
