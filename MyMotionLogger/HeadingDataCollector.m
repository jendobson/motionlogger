//
//  HeadingDataCollector.m
//  DataLogger
//
//  Created by Jennifer Dobson on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HeadingDataCollector.h"
#import "DeviceManager.h"
#import "CollectionEvent.h"

@implementation HeadingDataCollector

-(NSString *)sensorType
{
    return @"Heading";
}

-(NSString*)getDataNameFromKey:(EventDataName)dataNameKey
{
    NSString* dataName;
    if (dataNameKey == kHeadingHeadingAccuracy)
        dataName = @"headingAccuracy";
    if (dataNameKey == kHeadingMagneticHeading)
        dataName = @"magneticHeading";
    if (dataNameKey == kHeadingTimeStamp)
        dataName = @"timestamp";
    if (dataNameKey == kHeadingTrueHeading)
        dataName = @"trueHeading";
    if (dataNameKey == kHeadingX)
        dataName = @"x";
    if (dataNameKey == kHeadingY)
        dataName = @"y";
    if (dataNameKey == kHeadingZ)
        dataName = @"z";
        
    
    return dataName;
        
}
- (void) doAddHeadingToData: (CLHeading*)newHeading
{
    [self.measurements addObject:newHeading];
    [self.time addObject:[NSNumber numberWithDouble:[[NSProcessInfo processInfo] systemUptime]]];

}

-(void) doStartDataCollectionInEvent:(CollectionEvent*)event
{
   if ([event.deviceManager.locationManager headingAvailable])
   {
       [event.deviceManager.locationManager startUpdatingHeading];
   }
    else
    {    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Heading information is not available."
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

-(void) doStopDataCollectionInEvent:(CollectionEvent*)event
{
    [event.deviceManager.locationManager stopUpdatingHeading];
}



-(NSData*)bytesForDataField:(EventDataName)dataFieldName
{
    NSMutableData* returnData = [NSMutableData dataWithCapacity:self.measurements.count];
    
    
    if (dataFieldName == kHeadingTime)
        for (int i=0;i<self.time.count;i++)
        {
            double* timePtr;
            double time = [[self.time objectAtIndex:i] doubleValue];
            timePtr = &time;
            [returnData appendData:[NSData dataWithBytes:timePtr length:sizeof(double)]];
        }
    else 
    {
        double *dataBytes = malloc(sizeof(double)*self.measurements.count);
        for (int i=0;i<self.measurements.count;i++)
        {
            CLHeading *thisHeading = [self.measurements objectAtIndex:i];
            if (dataFieldName == kHeadingTimeStamp)
            {
                NSDate *theDate = [thisHeading valueForKey:@"timestamp"];
                dataBytes[i] = [theDate timeIntervalSinceReferenceDate];
            }
            else
            {
                NSString *dataName = [self getDataNameFromKey:dataFieldName];
                dataBytes[i] = [[thisHeading valueForKey:dataName] doubleValue];
            }
            
        }
        [returnData appendData:[NSData dataWithBytes:dataBytes length:sizeof(double)*self.measurements.count]];
        free(dataBytes);
    
    }
    
    
    return returnData;
    
}


@end


/*
 
 -(int) nBytesInData
 {
 return _nBytesInTrueHeading + _nBytesInMagneticHeading + _nBytesInHeadingAccuracy + _nBytesInX + _nBytesInY + _nBytesInZ;
 }
 
 -(int) nBytesInX
 {
 return self.nPoints*sizeof(CLHeadingComponentValue);
 }
 -(int) nBytesInY
 {
 return self.nPoints*sizeof(CLHeadingComponentValue);
 
 }
 -(int) nBytesInZ
 {
 return self.nPoints*sizeof(CLHeadingComponentValue);
 }
 -(int) nBytesInHeadingAccuracy
 {
 return self.nPoints*sizeof(CLLocationDirection);
 }
 -(int) nBytesInMagneticHeading
 {
 return self.nPoints*sizeof(CLLocationDirection);
 }
 -(int) nBytesInTrueHeading
 {
 return self.nPoints*sizeof(CLLocationDirection);
 }
 -(int) nbytesInDataField:(NSString*)dataField
 {
 int length = [super nbytesInDataField:dataField];
 
 if ([dataField isEqualToString:@"magneticHeading"])
 length = [self nBytesInMagneticHeading];
 if ([dataField isEqualToString:@"trueHeading"])
 length = [self nBytesInTrueHeading];
 
 if ([dataField isEqualToString:@"headingAccuracy"])
 length = [self nBytesInHeadingAccuracy];
 
 if ([dataField isEqualToString:@"x"])
 length = [self nBytesInX];
 
 if ([dataField isEqualToString:@"y"])
 length = [self nBytesInY];
 
 if ([dataField isEqualToString:@"z"])
 length = [self nBytesInZ];
 
 return length;
 
 }
*/