//
//  MyMotionLoggerTests.h
//  MyMotionLoggerTests
//
//  Created by Jennifer Dobson on 4/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@class Event, CollectionEvent;

@interface MyMotionLoggerTests : SenTestCase
@property (nonatomic, strong) Event* event;
@property (nonatomic, strong) CollectionEvent* collectionEvent;

@end
