//
//  MyMotionLoggerTests.m
//  MyMotionLoggerTests
//
//  Created by Jennifer Dobson on 4/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyMotionLoggerTests.h"

#import "CollectionEvent.h"
#import "Event.h"
#import "Event+Create.h"
#import "Utils.h"
#import "AppDelegate.h"
#import "GPSDataCollector.h"
#import "HeadingDataCollector.h"
#import "MagnetometerDataCollector.h"
#import "GyroscopeDataCollector.h"
#import "AccelerometerDataCollector.h"

@implementation MyMotionLoggerTests

@synthesize event = _event;
@synthesize collectionEvent = _collectionEvent;

- (void)setUp
{
    [super setUp];

    
    CollectionEvent *newCollectionEvent = [[CollectionEvent alloc] init];
    
    newCollectionEvent.collectsGPSData = YES;
    newCollectionEvent.collectsAccelerometerData = YES;
    newCollectionEvent.collectsGyroscopeData = YES;
    newCollectionEvent.collectsHeadingData = YES;
    newCollectionEvent.collectsMagnetometerData = YES;
    
    self.collectionEvent = newCollectionEvent;
    
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];

}

- (void)testExample
{

    self.collectionEvent.timeOutInterval = 10;
    [self.collectionEvent startCollection];
   /* 
    NSLog(@"Enabled? GPS: %i, Heading: %i, Gyro: %i, Mag: %i, Acc: %i",self.collectionEvent.deviceManager.gpsDataCollector.enabled,self.collectionEvent.deviceManager.headingDataCollector.enabled,self.collectionEvent.deviceManager.gyroscopeDataCollector.enabled,self.collectionEvent.deviceManager.magnetometerDataCollector.enabled,self.collectionEvent.deviceManager.accelerometerDataCollector.enabled);
*/
    NSLog(@"samplingFreq: %f, timeoutInterval: %f",self.collectionEvent.samplingFrequency,self.collectionEvent.timeOutInterval);
    
    // Let the collection run
    NSDate *runUntil = [NSDate dateWithTimeIntervalSinceNow:(self.collectionEvent.timeOutInterval+3)];
    [[NSRunLoop currentRunLoop] runUntilDate:runUntil];
        
    // Transfer data to Core Data Event
    NSManagedObjectContext *appcontext = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    self.event = [Event eventFromCollectionEvent:self.collectionEvent inManagedObjectContext:appcontext];
    self.collectionEvent = nil;
    /*
    NSArray* timestampDataArray = [Utils dataArrayFromData:self.event.gpsData.timestamp];
    NSArray* timeDataArray = [Utils dataArrayFromData:self.event.gpsData.time];
    NSArray *altDataArray = [Utils dataArrayFromData:self.event.gpsData.altitude];
    NSArray *courseDataArray = [Utils dataArrayFromData:self.event.gpsData.course];
    NSArray *horAccDataArray = [Utils dataArrayFromData:self.event.gpsData.horizontalAccuracy];
    NSArray *latitudeDataArray = [Utils dataArrayFromData:self.event.gpsData.latitude];
    NSArray* longitudeDataArray = [Utils dataArrayFromData:self.event.gpsData.longitude];
    NSArray* speedDataArray = [Utils dataArrayFromData:self.event.gpsData.speed];
    NSArray* vertAccDataArray = [Utils dataArrayFromData:self.event.gpsData.verticalAccuracy];
    
    // DO A CHEESE-CHECK ON THE GPS DATA
    NSLog(@"GPS: timestamp: %@,alt: %@, course: %@, horAcc: %@, lat: %@, long:%@, speed: %@, time:%@, vertAcc: %@",timestampDataArray,altDataArray,courseDataArray,horAccDataArray,latitudeDataArray,longitudeDataArray, speedDataArray, timeDataArray,vertAccDataArray);
    
    // TEST THAT ALL GPS DATA HAS SAME NUMBER OF POINTS
    STAssertEquals(timeDataArray.count, altDataArray.count,@"unequal data length.  %i ~= %i",timeDataArray.count, altDataArray.count);
    STAssertEquals(timeDataArray.count, courseDataArray.count,@"unequal data length.  %i ~= %i",timeDataArray.count, courseDataArray.count);
    STAssertEquals(timeDataArray.count, horAccDataArray.count,@"unequal data length.  %i ~= %i",timeDataArray.count, horAccDataArray.count);
    STAssertEquals(timeDataArray.count, latitudeDataArray.count,@"unequal data length.  %i ~= %i",timeDataArray.count, latitudeDataArray.count);
    STAssertEquals(timeDataArray.count, longitudeDataArray.count,@"unequal data length.  %i ~= %i",timeDataArray.count, longitudeDataArray.count);
    STAssertEquals(timeDataArray.count, speedDataArray.count,@"unequal data length.  %i ~= %i",timeDataArray.count, speedDataArray.count);
    STAssertEquals(timeDataArray.count, timestampDataArray.count,@"unequal data length.  %i ~= %i",timeDataArray.count, timestampDataArray.count);
    STAssertEquals(timeDataArray.count, vertAccDataArray.count,@"unequal data length.  %i ~= %i",vertAccDataArray.count, altDataArray.count);
    
    NSArray *headingAcc = [Utils dataArrayFromData:self.event.headingData.headingAccuracy];
    NSArray *magneticAcc = [Utils dataArrayFromData:self.event.headingData.magneticHeading];
    NSArray *headingTime = [Utils dataArrayFromData:self.event.headingData.time];
    NSArray *headingTimestamp = [Utils dataArrayFromData:self.event.headingData.timestamp];
    NSArray *headingTrueheading = [Utils dataArrayFromData:self.event.headingData.trueHeading];
    NSArray *headingX = [Utils dataArrayFromData:self.event.headingData.x];
    NSArray *headingY = [Utils dataArrayFromData:self.event.headingData.y];
    NSArray *headingZ = [Utils dataArrayFromData:self.event.headingData.z];
    
    // CHECK HEADING DATA
    NSLog(@"HEADING: hacc: %@, magacc: %@, time: %@, timestamp: %@, true heading: %@, x: %@, y: %@, z: %@",headingAcc,magneticAcc,headingTime,headingTimestamp,headingTrueheading,headingX,headingY,headingZ);
    
    STAssertEquals(headingAcc.count, magneticAcc.count, @"unequal data length.  %i ~= %i",headingAcc.count, magneticAcc.count);
    STAssertEquals(headingAcc.count, headingTime.count, @"unequal data length.  %i ~= %i",headingAcc.count, headingTime.count);
    STAssertEquals(headingAcc.count, headingTimestamp.count, @"unequal data length.  %i ~= %i",headingAcc.count, headingTimestamp.count);
    STAssertEquals(headingAcc.count, headingTrueheading.count, @"unequal data length.  %i ~= %i",headingAcc.count, headingTrueheading.count);
    STAssertEquals(headingAcc.count, headingX.count, @"unequal data length.  %i ~= %i",headingAcc.count, headingX.count);
    STAssertEquals(headingAcc.count, headingY.count, @"unequal data length.  %i ~= %i",headingAcc.count, headingY.count);
    STAssertEquals(headingAcc.count, headingZ.count, @"unequal data length.  %i ~= %i",headingAcc.count, headingZ.count);
    
    
    NSArray *gyroX = [Utils dataArrayFromData:self.event.gyroscopeData.xData];
    NSArray *gyroY = [Utils dataArrayFromData:self.event.gyroscopeData.yData];
    NSArray *gyroZ = [Utils dataArrayFromData:self.event.gyroscopeData.zData];
    NSArray *gyroTime = [Utils dataArrayFromData:self.event.gyroscopeData.time];
    
    NSLog(@"GYRO: xdata: %@, ydata: %@, zdata: %@, time: %@",gyroX,gyroY,gyroZ,gyroTime);
    
    STAssertEquals(gyroX.count, gyroY.count,@"unequal data length. %i != %i",gyroX.count,gyroY.count);
    STAssertEquals(gyroX.count, gyroZ.count,@"unequal data length. %i != %i",gyroX.count,gyroY.count);
    STAssertEquals(gyroX.count, gyroTime.count,@"unequal data length. %i != %i",gyroX.count,gyroY.count);
    //STAssertEqualsWithAccuracy((int)gyroX.count,(int) self.collectionEvent.samplingFrequency*self.collectionEvent.timeOutInterval,(int)100,@"bad number of points in gyro data");
    
    NSArray *magX = [Utils dataArrayFromData:self.event.magnetometerData.xData];
    NSArray *magY = [Utils dataArrayFromData:self.event.magnetometerData.yData];
    NSArray *magZ = [Utils dataArrayFromData:self.event.magnetometerData.zData];
    NSArray *magTime = [Utils dataArrayFromData:self.event.magnetometerData.time];
    
    NSLog(@"MAGNETOMETER: xdata: %@,  ydata:%@, zdata: %@, time: %@",magX,magY,magZ,magTime);
    
    STAssertEquals(magX.count, magY.count,@"unequal data length. %i != %i,",magX.count,magY.count);
    STAssertEquals(magX.count, magZ.count,@"unequal data length. %i != %i,",magX.count,magZ.count);
    STAssertEquals(magX.count, magTime.count,@"unequal data length. %i != %i,",magX.count,magTime.count);
   // STAssertEqualsWithAccuracy((int)magX.count, (int)self.collectionEvent.samplingFrequency*self.collectionEvent.timeOutInterval, (int)100,@"bad number of points in mag data");
    
    NSArray *accX = [Utils dataArrayFromData:self.event.accelerometerData.xData];
    NSArray *accY = [Utils dataArrayFromData:self.event.accelerometerData.yData];
    NSArray *accZ = [Utils dataArrayFromData:self.event.accelerometerData.zData];
    NSArray *accTime = [Utils dataArrayFromData:self.event.accelerometerData.time];
    
    NSLog(@"ACCELEROMETER: x: %@, y:%@, z:%@, time:%@",accX,accY,accZ,accTime);
    STAssertEquals(accX.count, accY.count,@"unequal data length. %i != %i,",accX.count,accY.count);
    STAssertEquals(accX.count, accZ.count,@"unequal data length. %i != %i,",accX.count,accZ.count);
    STAssertEquals(accX.count, accTime.count,@"unequal data length. %i != %i,",accX.count,accTime.count);
    //STAssertEqualsWithAccuracy((int)accX.count,(int)self.collectionEvent.samplingFrequency*self.collectionEvent.timeOutInterval, (int)100,@"bad number of points in acc data");
   
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"]; 
    request.fetchBatchSize = 20;
    request.fetchLimit = 100;
    
    NSSortDescriptor *sortDescriptor =[NSSortDescriptor sortDescriptorWithKey:@"startTime"
                                  ascending:YES];
    
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor]; 
    
    NSError *error;
    NSArray *events = [appcontext executeFetchRequest:request error:&error];
    
    NSLog(@"number of events: %i",events.count);
    
    //NSLog(@"all events: %@",events);
    
    NSLog(@"the error: %@",error);
    
    
    // Check that Event Data Parsing API give correct data
    STAssertEqualObjects(accX, [self.event getData:kAccelerometerXData],@"AccX not correct");
    
    // Delete this event
    [appcontext deleteObject:self.event];
    [appcontext save:&error];
    self.event = nil;
    NSLog(@"number of events: %i",events.count); // no change since events array is not changed
     */
}

@end
